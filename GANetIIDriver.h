/*--------------------------------------------------------------------------------------------------

  Name         :  GANetIIDriver.h

  Description  :  GANet-II/IEBus Driver

  Copyright    :  (c) 2012 Crazy Giraffe Software
  
----------------------------------------------------------------------------------------------------*/
#include <avr/io.h>

#ifndef _GANET_DRIVER_H_
#define _GANET_DRIVER_H_

#ifndef GANET_VERSION "1.0"

// Device Types
typedef enum
{
    // CD Player
    GANET_DEVICE_CD = 1,

    // XM Radio 
    GANET_DEVICE_XM = 2,

    // USB Interface
    GANET_DEVICE_USB = 3,
}
GANET_DEVICE_TYPE;

// Callback prototypes
typedef void (*GANetStateChange) (GANetStateChangeType* pState);
typedef void (*GANetsOnError) (GANetErrorMessageType* pError);
typedef uint8_t (*GANetProcessDataWrite) (GANetMessageType* pMessage);
typedef uint8_t (*GANetProcessDataRead) (GANetMessageType* pMessage);

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetInit

  Description  :  Initialize this library to read/write from the GANet-II Bus.

  Argument(s)  :  deviceType          -> Type of device to emulate
                  stateChangeCallback -> Callback to handle state changes.
                  onErrorCallback     -> Callback to handle error conditions.
                  dataWriteCallback   -> Callback to handle write commnds.
                  dataReadCallback    -> Callback to handle read commands.

  Return value :  TRUE if initialize, FALSE is not.

--------------------------------------------------------------------------------------------------*/
uint8_t GANetInit(
    GANET_DEVICE_TYPE deviceType,
    GANetStateChange stateChangeCallback,
    GANetOnError onErrorCallback,
    GANetProcessDataWrite dataWriteCallback,
    GANetProcessDataRead dataReadCallback
    );

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
void GANetProcess(void);

#endif // _GANET_DRIVER_H_
