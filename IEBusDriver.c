/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusDriver.c

  Description  :  IEBus driver module.

  Author       :  Rob Ward (info@crazygiraffesoftware.com)

  Copyright    :  (c) 2012 Crazy Giraffe Software

  Credits:     :  Adapted from AvcLanDriver.c
                  by Louis Frigon (info@sigmaobjects.com)
                  (c) 2007 SigmaObjects

----------------------------------------------------------------------------------------------------

                                          IEBus Theory

    General:
    ------------
    The IEBus is designed to enable the data transmission between devices in a small-scale
    digital data transmission system.


    Most of this info comes from the �PD72042B datasheet from NEC/Renasa.  However, this
    datasheet is no longer available on the NEC/Renesas site.  You can find it via your favorite
    search engine.

    Sadly, Digikey (and many others) don't carry this chip.  A few others have been able to track some
    samples down. In the end, using this chip has one major limitation: it cannot be used to sniff the
    bus due to the fact that it will only receieve messages for it's specific address.
    See: http://am.renesas.com/support/faqs/faq_results/Q1000000-Q9999999/commu/iebus/iebus_010gl.jsp

    Additional sources are noted below.  Much of this work is derived from Louis Frigon, without which
    this work would not be possible.  Louis has stated his work would not be possible without the work
    of Marcin Slonicki. Thanks to Louis and Marcin.

    See: http://www.sigmaobjects.com/toyota/
    See: http://www.softservice.com.pl/corolla/avc/simpleaux.php

    Electrical:
    ------------
    IEBus uses a differential line, labeled BUS+ and BUS-.

    Communication scale
    Number of units         :   50 max
    Cable length            :   150 m max (when twisted-pair cable is used <resistance 0.1 O/m or less>)
    Load capacity           :   8000 pF max <between BUS- and BUS+>, fX = 6 MHz
                                7100 pF max <between BUS- and BUS+>, fX = 6.29 MHz
    Terminating resistance  :   120 ohm

    As a protective resistance, connect a 180 ohm resistor in series with the BUS-
    and BUS+ pins.

    The input high voltage/input low voltage defines a potential difference (voltage) between the
    BUS+ and BUS- pins. If this difference is 120 mV or more, the high level is recognized; if it
    is 20 mV or less, the low level is recognized.

    This will be refer to as "high level" and "low level".

    On the other hand, the common mode input voltage, high/low voltage defines a potential difference
    (voltage) between BUS+ and ground or between BUS- and ground. In order to be recognized as the
    high level, BUS+ and BUS- must be at 1.0 V to VDD - 1.0 V and the potential difference between
    BUS+ and BUS- must be 120 mV or more. In order to be recognized as the low level, BUS+ and BUS-
    must be at 0 V to VDD and the potential difference between BUS+ and BUS- must be 20 mV or less.

            |
            |
        VDD | ------------------------------------
            |               ^
            |               |   1V or more
            |               |
            |  Low       ___v_____________________
            |  Level    /                ^
            |____v_____/       High      |
            |__________        Level    120 mV
            |    ^     \                 |
            |  20 mV    \________________v________
            |               ^
            |               |  1 V
            |               |  or more
        0 V |_______________v_____________________


    See: http://am.renesas.com/support/faqs/faq_results/Q1000000-Q9999999/commu/iebus/iebus_013gl.jsp

    CAN line drivers can work to drive the bus but not to receive the bus: CAN uses different voltage
    levels, 500mv (and less) for a recessive level and 900 mv (or more) for a dominate level. Therefore,
    a CAN reciever will not properly detect the high level condition of IEBUS.

    Recommended line driver chip: HA12187FP.  Sadly, Digikey (and many others) don't carry this chip.
    See: http://documentation.renesas.com/doc/products/mpumcu/apn/rej06b0488_h8s.pdf.

    Others have implemented using seperate circuits for send and receive.  The receive circuit usually
    consists of a comparator designed to detect the 20mv/120mv difference.  This can be done using an AVR's\
    analog comparator (as in Louis' work) or an dedicated component (Martin's work).  Alex from
    flux242.blogspot.com as also used the dedicated component approach.
    See: http://flux242.blogspot.com/2007/09/avclan-iebus-sniffer-and-device.html

    Bit Format:
    ------------
    Fig. 2-4 illustrates the bits that constitute an IEBus communication frame.
    Fig. 2-4 IEBus Bit Format (Concept)

    Logic '1' ------------,                ,----------------,-----------,                ,----------------,-------
                          |                |                |           |                |                |
                          |                |                |           |                |                |
    Logic '0'             '----------------'----------------'           '----------------'----------------'

              |-----------|----------------|----------------|-----------|----------------|----------------|

               Preparation  Synchronization      Data         Preparation Synchronization      Data
               period       period               period       period       period              period

    Logic 1: The potential difference between the bus lines (the BUS+ and BUS- pins)
             is 20 mV or less (low level).

    Logic 0: The potential difference between the bus lines (the BUS+ and BUS- pins)
             is 120 mV or more (high level).

    Preparation period:         First and subsequent low-level (logic 1) periods
    Synchronization period :    Next high-level (logic 0) period
    Data period :               Period in which a bit value is indicated (logic 1 = low level, logic 0 = high level)

    The synchronization and data periods are almost equal in duration.

    For the IEBus, synchronization is established for each bit. The specifications of the total time required for a bit
    and the duration of each period allotted within the bit vary depending on the type of the transmission bits, and
    whether the unit is a master or slave.

    To eliminate confusion between Logic '0' and a '0' being transferred across the bus, I'm going to use the terms
    dominant and recessive (borrowed from CAN): dominant will be a "logic 0" (120 mv) and recessive will be "logic 1" (20 mv).
    Subsequent mentions of '0' and '1' will refer to values of bits being transferred and not the BUS differential.

    Speed:
    ------------
    Two modes, each offering different transmission speeds, can be selected.

            fX = 6 MHz          fX = 6.29 MHz       Maximum number of bytes
                                                    transmitted (bytes/frame)
    Mode 0  Approx. 3.9 Kbps    Approx. 4.1 Kbps    16
    Mode 1  Approx. 17 Kbps     Approx. 18 Kbps     32

    Frame Format:
    ------------
    A communication frame consists of the following bits:

      1   Start bit
      1   Broadcast

      12  Master address
      1   Parity
      12  Slave address
      1   Parity
      1   * Acknowledge * (read below)

      4   Control
      1   Parity
      1   * Acknowledge * (read below)

      8   Payload length (n)
      1   Parity
      1   * Acknowledge * (read below)

      8   Data
      1   Parity
      1   * Acknowledge * (read below)
      repeat 'n' times

    A parity bit is used to check for errors in the transmission data. A parity bit is added to
    the master address bits, slave address bits, control bits, data-length bits, and data bits.
    Even parity is used. If the number of 1�s in the data is odd, the parity bit is set to 1.
    If the number of 1�s in the data is even, the parity bit is set to 0.

    The acknowledge bit is defined as follows:
    0: Indicates that transmission data has been recognized. (ACK)
    1: Indicates that no transmission data has been recognized. (NAK)

    In point-to-point communication, sender issues an ack bit with value '1'. Receiver
    upon acking will extend the bit until it looks like a '0' on the bus. In broadcast
    mode, receiver disregards the bit.

    For broadcast, the acknowledge bit included in the frame but ignored by the sender.

    Timing:
    ------------
    From Fig. 2-1 Transmission Signal Format: (When fX = 6 MHz)

    Transmission    Header->Data Length fields      Data field(s)
    time
    Mode 0          Approx. 7330 us                 Approx. 1590 �~ N us
    Mode 1          Approx. 2090 us                 Approx. 410 �~ N us


    The specific timing values are not clear from the data sheet.  The following examples
    assume Mode 1 @ 6.29 MHz and come from from from Louis' work on AVCLanDriver:

        A rising edge (high level) indicates a new bit. The duration of the high level tells whether
        it is a start bit (~165 us), a bit '0' (~30 us) or a bit '1' (~20 us). A normal bit length
        is close to 40 us.

                          |<---- Bit '0' ---->|<---- Bit '1' ---->|
        Physical '1'      ,---------------,   ,---------,         ,---------
        Logic '0'         ^               |   ^         |         ^
                          ^               |   ^         |         ^
        Physical '0' -----'               '---'         '---------'--------- Idle low
        Logic '1'         |---- 32 us ----| 7 |- 20 us -|- 19 us -|

        A bit '1' is typically 20 us high followed by 19 us low.

        A bit '0' is typically 32 us high followed by 7 us low. A bit '0' is dominant i.e. it takes
        precedence over a '1' by extending the pulse.

        A start bit is typically 165 us high followed by 30 us low.

    IEBus is also used by Acura and Honda vehicles.  There is an Acura Music Link box that
    uses a uPD72042B and communicates with an iPod.  Steve at nuxx.net cataloged the components
    on the Acura board and took some excellent photos.  The details are so good that you can view
    the traces and vias.  With the exception that I'm making an assumption about what is under the
    uPD72042B, it appears that there is a 6.29 mHZ crystal connected to the X0 and X1 pins (6&7) on
    the uPD72042B.  This lines up well with the work from Louis below: extrapolation of 410 us at
    6 Mhz is 390 us at 6.29 Mhz, which matches the timing Loius and Marcin are seeing.
    See: https://nuxx.net/wiki/Honda_/_Acura_Music_Link_(Technical)

    Misc:
    ------------
    Renesas provies some information about IEBus in pratical use in an application note.  Among other things,
    it specifies a line driver chip, termination resistance and logic necessary to ocmmunicate with IEBus when
    using a Renesas micro.

    See: http://documentation.renesas.com/doc/products/mpumcu/apn/rej06b0488_h8s.pdf

    Notes on this implmentation:
    ------------
    In order to make IEBus more accesible to the hobbiest community, this implmentation is built on an AVR
    8-bit processor, which is easily and cheaply programmed.

    This implmentation refactors the code into AVC, GA-NET II and IEBus layers to allow for use in multiple
    application, since I currently own Honda Civic and my wife told me she's going to buy Toyota Highlander
    next year since she wants to get rid of the mini-van.

    In addition, the additional IEBus logic consumes more cycles that other implmentations so in order
    to take maximum advantage of available cycles, the read/write logic is implemented in interrupts.
    This allows the driver to process messages while a bit is being transmitted on the bus: the
    processor is not tied up while the IEBus is in the high state.

--------------------------------------------------------------------------------------------------*/
#include "IEBusDriver.h"
#include <string.h>
#include <avr/interrupt.h>

// Field lengths (number of bits)
#define IEBUS_START_BIT_LENGTH             1
#define IEBUS_BROADCAST_FIELD_LENGTH       1
#define IEBUS_PARITY_FIELD_LENGTH          1
#define IEBUS_ACK_FIELD_LENGTH             1
#define IEBUS_MASTER_ADDR_FIELD_LENGTH    12
#define IEBUS_SLAVE_ADDR_FIELD_LENGTH     12
#define IEBUS_CONTROL_FIELD_LENGTH         4
#define IEBUS_DATA_SIZE_FIELD_LENGTH       8
#define IEBUS_DATA_FIELD_LENGTH            8

// Data sentinal values.  IEBus only uses 12-bit
// data so this will not conflict with any fields.
#define IEBUS_DATA_START_BIT    0xffff
#define IEBUS_DATA_TIMEOUT_BIT  0xfffe

// For group broadcast, the number of a target group is indicated by the
// high-order 4 bits of the slave address
#define IEBUS_GROUP_BROADCAST_MASK 0xF00

// Default parity. Even parity is used.
// If the number of 1�s in the data is odd, the parity bit is set to 1.
// If the number of 1�s in the data is even, the parity bit is set to 0.
#define IEBUS_PARITY_DEFAULT 0

// The acknowledge pattern is tricky: the sender shall drive the bus for the equivalent
// of a bit '1' then release the bus and listen. At this point the target shall have
// taken over the bus maintaining the pulse until the equivalent of a bit '0' is formed.
#define IEBUS_DATA_TX_ACK  1
#define IEBUS_DATA_RX_ACK  0
#define IEBUS_DATA_RX_NACK 1

// The read pattern works the same way: the sender shall drive the bus for the equivalent
// of a bit '1' then release the bus and listen.
#define IEBUS_DATA_TX_READ_BYTE 0xff
#define IEBUS_DATA_TX_READ_BIT  1

// Meanings of the Control Bits
// 0H 0 0 0 0 Read slave status (SSR)
// 1H 0 0 0 1 Undefined
// 2H 0 0 1 0 Undefined
// 3H 0 0 1 1 Read data and locking
// 4H 0 1 0 0 Read lock address (low-order 8 bits)
// 5H 0 1 0 1 Read lock address (high-order 4 bits)
// 6H 0 1 1 0 Read slave status (SSR) and unlocking
// 7H 0 1 1 1 Read data
// 8H 1 0 0 0 Undefined
// 9H 1 0 0 1 Undefined
// AH 1 0 1 0 Write command and locking
// BH 1 0 1 1 Write data and locking
// CH 1 1 0 0 Undefined
// DH 1 1 0 1 Undefined
// EH 1 1 1 0 Write command
// FH 1 1 1 1 Write data
// master transmission (when bit 3 of the control bits is 1) or
// master reception (when bit 3 of the control bits is 0)
#define IEBUS_CONTROL_WRITE_MASK        0x08
#define IEBUS_CONTROL_WRITE_VALID_MASK  0x0A
#define IEBUS_CONTROL_NO_LOCK_MASK      0x04

// Status byte
// Bit Value Meaning
// Bit 0    0 The slave transmission buffer is empty.
//          1 The slave transmission buffer is not empty.
// Bit 1    0 The slave reception buffer is empty.
//          1 The slave reception buffer is not empty.
// Bit 2    0 The unit is not locked.
//          1 The unit is locked.
// Bit 3    0 Fixed at 0
// Bit 4    0 Slave transmission disabled
//          1 Slave transmission enabled
// Bit 5    0 Fixed at 0
// Bit 7    00 Mode 0
// Bit 6    01 Mode 1
//          10 Reserved for future expansion
//
// "Bits 7 and 6 are currently fixed to 10 in the hardware of the �PD72042B."
//
#define IEBUS_STATUS_LOCK_MASK      0x04
#define IEBUS_STATUS_LOCK_SHIFT     2
#define IEBUS_FIXED_STATUS_MODE     0x80

// Macros for bus handling
#define IEBUS_BUS_IS_HIGH_LEVEL     (bit_is_set(IEBUS_RX_PIN, IEBUS_RX_IO))
#define IEBUS_BUS_IS_LOW_LEVEL      (bit_is_clear(IEBUS_RX_PIN, IEBUS_RX_IO))
#define IEBUS_BUS_SET_HIGH_LEVEL    IEBUS_TX_PORT |= _BV(IEBUS_TX_IO)
#define IEBUS_BUS_SET_LOW_LEVEL     IEBUS_TX_PORT &= ~_BV(IEBUS_TX_IO)

// Macros for timer handling. These macros end with ";" and as they are
// not intended to be used in conditionals.
//
// Note: Compare A should always be greater than Compare B since the timer will
// reset in CTC mode based on the Compare A value.
#define IEBUS_TIMER_RESET           IEBUS_TCNT = 0;
#define IEBUS_TIMER_SET_COMPA(x)    IEBUS_OCRAH = (x >> 8); IEBUS_OCRAL = (x & 0xff);
#define IEBUS_TIMER_SET_COMPB(x)    IEBUS_OCRBH = (x >> 8); IEBUS_OCRBL = (x & 0xff);

// True/false
#ifndef FALSE
#define FALSE                   0
#endif
#ifndef TRUE
#define TRUE                    (!FALSE)
#endif

// Values used to track state of the bus.
typedef enum
{
    // No bus activity.
    IEBUS_BUS_STATE_IDLE = 0,

    // We are transmitting data on the bus.
    IEBUS_BUS_STATE_TX,

    // We are recieving data from the bus.
    IEBUS_BUS_STATE_RX,
}
IEBUS_BUS_STATE;

// Values used to track frame states for Rx and Tx.
typedef enum
{
    IEBUS_FRAME_STATE_IDLE = 0,
    IEBUS_FRAME_STATE_START_BIT,
    IEBUS_FRAME_STATE_BROADCAST_FIELD,
    IEBUS_FRAME_STATE_MASTER_ADDR_FIELD,
    IEBUS_FRAME_STATE_MASTER_ADDR_PARITY,
    IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD,
    IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY,
    IEBUS_FRAME_STATE_SLAVE_ADDR_ACK,
    IEBUS_FRAME_STATE_CONTROL_FIELD,
    IEBUS_FRAME_STATE_CONTROL_PARITY,
    IEBUS_FRAME_STATE_CONTROL_ACK,
    IEBUS_FRAME_STATE_DATA_SIZE_FIELD,
    IEBUS_FRAME_STATE_DATA_SIZE_PARITY,
    IEBUS_FRAME_STATE_DATA_SIZE_ACK,
    IEBUS_FRAME_STATE_DATA_FIELD,
    IEBUS_FRAME_STATE_DATA_PARITY,
    IEBUS_FRAME_STATE_DATA_ACK,
}
IEBUS_FRAME_STATE;

#define IEBUS_FRAME_MAX IEBUS_FRAME_STATE_DATA_ACK + 1

// Bus timing variables. These hold the counter values
// necessary to read or write different bus states.
uint16_t                IEBusBusIdleTime;
uint16_t                IEBusReadBitIsZero;
uint16_t                IEBusReadBitIsStart;
uint16_t                IEBusWriteStartBit;
uint16_t                IEBusStartBitLength;
uint16_t                IEBusWriteZeroBit;
uint16_t                IEBusWriteOneBit;
uint16_t                IEBusBitLength;

// The state of the bus.
IEBUS_BUS_STATE         IEBusBusState;
volatile uint8_t        IEBusDataAvailable;

// Common frame information.
uint8_t                 IEBusExpectedFieldBitLength[IEBUS_FRAME_MAX];
IEBUS_FIELD_TYPE        IEBusFieldType[IEBUS_FRAME_MAX];

// Receive variables.
IEBUS_FRAME_STATE       IEBusRxFrameState;
IEBusMessageType        IEBusRxMessage;
uint8_t                 IEBusRxMessageIsWrite;
uint16_t                IEBusRxData;
uint8_t                 IEBusRxDataBitIndex;
uint8_t                 IEBusRxDataTotalBitCount;
uint8_t                 IEBusRxParityBit;
uint8_t                 IEBusRxResponseData;
uint8_t                 IEBusRxResponseDataBitIndex;
uint8_t                 IEBusRxResponseDataTotalBitCount;
uint8_t                 IEBusRxResponseParityBit;
uint8_t                 IEBusRxMessageFromMe;
uint8_t                 IEBusRxMessageToMe;
uint8_t                 IEBusRxForMe;
uint8_t                 IEBusRxWillLock;
uint8_t                 IEBusRxWillUnlock;
uint16_t                IEBusRxDataFieldsSent;
uint16_t                IEBusRxDataFieldOffset;

// Transmit variables.
IEBUS_FRAME_STATE       IEBusTxFrameState;
IEBusMessageType        IEBusTxMessage;
uint8_t                 IEBusTxMessageIsPending;
uint8_t                 IEBusTxMessageIsWrite;
uint16_t                IEBusTxData;
uint8_t                 IEBusTxDataBitIndex;
uint8_t                 IEBusTxDataTotalBitCount;
uint8_t                 IEBusTxParityBit;
uint8_t                 IEBusTxAckBit;
uint16_t                IEBusTxDataFieldsSent;
uint16_t                IEBusTxDataFieldOffset;
uint16_t                IEBusTxOriginalDataSize;

// Addresses and locking state.
uint16_t                IEBusMyAddress;
uint16_t                IEBusLockAddress;
uint8_t                 IEBusLockStatus;

// Callbacks function pointers.  Make sure these are initialized to NULL.
IEBusStateChange        IEBusStateChangeCallback = 0;
IEBusOnError            IEBusOnErrorCallback = 0;
IEBusProcessDataWrite   IEBusDataWriteCallback = 0;
IEBusProcessDataRead    IEBusRxDataCallback = 0;

// Forward declaration.
void IEBusTxResetState(void);
uint8_t IEBusTxGetNextBit(void);
void IEBusRxResetState(void);
uint8_t IEBusRxShouldSendBit(void);
uint8_t IEBusRxGetNextBit(void);

// This function is called everytime a bit is received to determine how to handle the response
// from the bus (either slave or master echo). This function assumes it is being
// called only in the transmit case.
void IEBusTxProcessBit(void);

// This function is called everytime a bit is received to determine how to handle the response
// from the bus (either slave or master echo). This function assumes it is being
// called only in the transmit and recieve case.
void IEBusRxProcessBit(void);

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusInit

  Description  :  Initialize this library to read/write from the IEBbus.

  Argument(s)  :  stateChangeCallback -> Callback to handle state changes.
                  onErrorCallback     -> Callback to handle error conditions.
                  dataWriteCallback   -> Callback to handle write commnds.
                  dataReadCallback    -> Callback to handle read commands.

  Return value :  TRUE if initialize, FALSE is not.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusInit(
    IEBusStateChange stateChangeCallback,
    IEBusOnError onErrorCallback,
    IEBusProcessDataWrite dataWriteCallback,
    IEBusProcessDataRead dataReadCallback
    )
{
    // Init timer used to drive/check IEBus
    // No PWM, count to max
    IEBUS_TCCRA = 0;

    // Clock to produce 1us or better
    // Note: scaling is x10 to avoid rounidng error
    uint8_t clockScaling = 10;
    if (F_CPU < 8000000)
    {
        // Timer clock is Clk-IO;
        IEBUS_TCCRB = IEBUS_TCCRB_CLK
        clockScaling = F_CPU / 100000;
    }
    else
    {
        // Timer clock is Clk-IO/8
        IEBUS_TCCRB = IEBUS_TCCRB_CLK8;
        clockScaling = F_CPU / 800000;
    }

    // Calculate bit threshold times: these are the average of 1 and 0 and 0 and start bits.
    // The bus idle time is the greater of the start bit low level or total bit time
    // plus a few ms to esure the bus is truly free.
    IEBusReadBitIsZero = ((IEBUS_BIT_0_HIGH_LENGTH_MS - IEBUS_BIT_1_HIGH_LENGTH_MS) / 2) * clockScaling / 10;
    IEBusReadBitIsStart = ((IEBUS_START_BIT_HIGH_LENGTH_MS - IEBUS_BIT_0_HIGH_LENGTH_MS) / 2) * clockScaling / 10;
    IEBusBusIdleTime = (IEBUS_START_BIT_LENGTH_MS - IEBUS_START_BIT_HIGH_LENGTH_MS);
    IEBusBusIdleTime = (IEBUS_BIT_LENGTH_MS > IEBusBusIdleTime) ? IEBUS_BIT_LENGTH_MS: IEBusBusIdleTime;
    IEBusBusIdleTime = (IEBusBusIdleTime + 10) * clockScaling / 10;
    IEBusWriteStartBit = IEBUS_START_BIT_HIGH_LENGTH_MS * clockScaling / 10;
    IEBusStartBitLength = IEBUS_START_BIT_LENGTH_MS * clockScaling / 10;
    IEBusWriteZeroBit = IEBUS_BIT_0_HIGH_LENGTH_MS * clockScaling / 10;
    IEBusWriteOneBit = IEBUS_BIT_1_HIGH_LENGTH_MS * clockScaling / 10;
    IEBusBitLength = IEBUS_BIT_LENGTH_MS * clockScaling / 10;

    // Initialize bus state
    IEBusBusState = IEBUS_BUS_STATE_IDLE;
    IEBusDataAvailable = FALSE;

    // Initialize Rx state
    IEBusRxFrameState = IEBUS_FRAME_STATE_IDLE;
    IEBusRxMessage.Broadcast = FALSE;
    IEBusRxMessage.MasterAddress = IEBUS_INVALID_ADDR;
    IEBusRxMessage.SlaveAddress = IEBUS_INVALID_ADDR;
    IEBusRxMessage.Control = IEBUS_CONTROL_GENERIC_WRITE;
    IEBusRxMessage.DataSize = 0;
    IEBusRxMessage.Data[0] = 0;
    IEBusRxMessageIsWrite = FALSE;
    IEBusRxData = 0;
    IEBusRxDataBitIndex = 0;
    IEBusRxDataTotalBitCount = 0;
    IEBusRxParityBit = IEBUS_PARITY_DEFAULT;
    IEBusRxResponseData = 0;
    IEBusRxResponseDataBitIndex = 0;
    IEBusRxResponseDataTotalBitCount = 0;
    IEBusRxResponseParityBit = IEBUS_PARITY_DEFAULT;
    IEBusRxForMe = FALSE;
    IEBusRxMessageFromMe = FALSE;
    IEBusRxMessageToMe = FALSE;
    IEBusRxWillLock = FALSE;
    IEBusRxWillUnlock = FALSE;
    IEBusRxDataFieldsSent = 0;
    IEBusRxDataFieldOffset = 0;

    // Initialize Tx state
    IEBusTxFrameState = IEBUS_FRAME_STATE_IDLE;
    IEBusTxMessage.Broadcast = FALSE;
    IEBusTxMessage.MasterAddress = IEBUS_INVALID_ADDR;
    IEBusTxMessage.SlaveAddress = IEBUS_INVALID_ADDR;
    IEBusTxMessage.Control = IEBUS_CONTROL_GENERIC_WRITE;
    IEBusTxMessage.DataSize = 0;
    IEBusTxMessage.Data[0] = 0;
    IEBusTxMessageIsPending = FALSE;
    IEBusTxMessageIsWrite = FALSE;
    IEBusTxData = 0;
    IEBusTxDataBitIndex = 0;
    IEBusTxDataTotalBitCount = 0;
    IEBusTxParityBit = IEBUS_PARITY_DEFAULT;
    IEBusTxAckBit = IEBUS_DATA_TX_ACK;
    IEBusTxDataFieldsSent = 0;
    IEBusTxDataFieldOffset = 0;
    IEBusTxOriginalDataSize = 0;

    // Initialize addresses and locking state.
    IEBusMyAddress = IEBUS_INVALID_ADDR;
    IEBusLockAddress = IEBUS_INVALID_ADDR;
    IEBusLockStatus = FALSE;

    // Store callbacks
    IEBusStateChangeCallback = stateChangeCallback;
    IEBusOnErrorCallback = onErrorCallback;
    IEBusDataWriteCallback = dataWriteCallback;
    IEBusRxDataCallback = dataReadCallback;

    // Check timing for out of range.
    // The timer code assumes an 8-bit timer and a max
    // clock of 20Mhz and operates for IEBus mode 1.
    //
    if ((IEBusReadBitIsStart > IEBUS_TIMER_MAX_VALUE) ||
        (IEBusWriteStartBit > IEBUS_TIMER_MAX_VALUE) ||
        (IEBusStartBitLength > IEBUS_TIMER_MAX_VALUE))
    {
        if (IEBusOnErrorCallback)
        {
            IEBusErrorMessageType error = { IEBUS_ERROR_OUT_OF_RANGE, IEBUS_START_BIT_FIELD };
            IEBusOnErrorCallback(&error);
        }
        return FALSE;
    }

    if ((IEBusReadBitIsZero > IEBUS_TIMER_MAX_VALUE) ||
        (IEBusBusIdleTime > IEBUS_TIMER_MAX_VALUE) ||
        (IEBusWriteZeroBit > IEBUS_TIMER_MAX_VALUE) ||
        (IEBusWriteOneBit > IEBUS_TIMER_MAX_VALUE) ||
        (IEBusBitLength > IEBUS_TIMER_MAX_VALUE))
    {
        if (IEBusOnErrorCallback)
        {
            IEBusErrorMessageType error = { IEBUS_ERROR_OUT_OF_RANGE, IEBUS_BROADCAST_FIELD };
            IEBusOnErrorCallback(&error);
        }
        return FALSE;
    }

    // Load expected bit counts into array for easy lookup.
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_IDLE]               = 0;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_START_BIT]          = IEBUS_START_BIT_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_BROADCAST_FIELD]    = IEBUS_BROADCAST_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_MASTER_ADDR_FIELD]  = IEBUS_MASTER_ADDR_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_MASTER_ADDR_PARITY] = IEBUS_PARITY_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD]   = IEBUS_SLAVE_ADDR_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY]  = IEBUS_PARITY_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_SLAVE_ADDR_ACK]     = IEBUS_ACK_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_CONTROL_FIELD]      = IEBUS_CONTROL_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_CONTROL_PARITY]     = IEBUS_PARITY_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_CONTROL_ACK]        = IEBUS_ACK_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_DATA_SIZE_FIELD]    = IEBUS_DATA_SIZE_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_DATA_SIZE_PARITY]   = IEBUS_PARITY_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_DATA_SIZE_ACK]      = IEBUS_ACK_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_DATA_FIELD]         = IEBUS_DATA_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_DATA_PARITY]        = IEBUS_PARITY_FIELD_LENGTH;
    IEBusExpectedFieldBitLength[IEBUS_FRAME_STATE_DATA_ACK]           = IEBUS_ACK_FIELD_LENGTH;

    // Load field types into array for easy lookup.
    IEBusFieldType[IEBUS_FRAME_STATE_IDLE]               = IEBUS_START_BIT_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_START_BIT]          = IEBUS_START_BIT_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_BROADCAST_FIELD]    = IEBUS_BROADCAST_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_MASTER_ADDR_FIELD]  = IEBUS_MASTER_ADDR_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_MASTER_ADDR_PARITY] = IEBUS_MASTER_ADDR_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD]   = IEBUS_SLAVE_ADDR_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY]  = IEBUS_SLAVE_ADDR_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_SLAVE_ADDR_ACK]     = IEBUS_SLAVE_ADDR_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_CONTROL_FIELD]      = IEBUS_CONTROL_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_CONTROL_PARITY]     = IEBUS_CONTROL_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_CONTROL_ACK]        = IEBUS_CONTROL_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_DATA_SIZE_FIELD]    = IEBUS_DATA_SIZE_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_DATA_SIZE_PARITY]   = IEBUS_DATA_SIZE_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_DATA_SIZE_ACK]      = IEBUS_DATA_SIZE_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_DATA_FIELD]         = IEBUS_DATA_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_DATA_PARITY]        = IEBUS_DATA_FIELD;
    IEBusFieldType[IEBUS_FRAME_STATE_DATA_ACK]           = IEBUS_DATA_FIELD;

    // Configure IO.
    IEBUS_BUS_SET_LOW_LEVEL;
    IEBUS_TX_DDR |= _BV(IEBUS_TX_IO);
    IEBUS_RX_DDR &= ~_BV(IEBUS_RX_IO);

    // Success
    return TRUE;
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusSetAddress

  Description  :  Set the address for which to send and receive messages.

  Argument(s)  :  address -> address for which to receive messages.

  Return value :  TRUE if address set, FALSE if not.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusSetAddress(uint16_t address)
{
    if ((IEBUS_INVALID_ADDR == address) ||
        (IEBUS_BROADCAST_ADDR > address))
    {
        IEBusMyAddress = address;
        return TRUE;
    }
    else if (IEBusOnErrorCallback)
    {
        IEBusErrorMessageType error = { IEBUS_ERROR_OUT_OF_RANGE, IEBUS_MASTER_ADDR_FIELD };
        IEBusOnErrorCallback(&error);
    }
    
    return FALSE;
}   

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusSendMessage

  Description  :  Sends the message to the IEBus.

  Argument(s)  :  pTxMessage -> message to send.

  Return value :  TRUE if successful queued for sending, else FALSE.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusSendMessage(IEBusMessageType* pTxMessage)
{
    // Default to no error
    IEBusErrorMessageType error = { IEBUS_ERROR_NONE };

    // Sanity check.
    if (!pTxMessage)
    {
        error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
        error.Field = IEBUS_START_BIT_FIELD;
        goto error;
    }

    // Make sure we have a valid master address either in the message
    // or assigned to the library.
    if ((pTxMessage->MasterAddress == IEBUS_MY_ADDR) &&
        (IEBusMyAddress == IEBUS_INVALID_ADDR))
    {
        error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
        error.Field = IEBUS_MASTER_ADDR_FIELD;
        goto error;
    }
    if ((pTxMessage->MasterAddress != IEBUS_MY_ADDR) &&
        (pTxMessage->MasterAddress >= IEBUS_BROADCAST_ADDR))
    {
        error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
        error.Field = IEBUS_MASTER_ADDR_FIELD;
        goto error;
    }

    // Make sure we have a valid slave address.
    if (pTxMessage->SlaveAddress >= IEBUS_BROADCAST_ADDR)
    {
        error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
        error.Field = IEBUS_SLAVE_ADDR_FIELD;
        goto error;
    }

    // Note: 0x00 is 256 bytes to transmit.  Since the field is only
    // 8 bits, this is the maximum data size.
    if ((pTxMessage->DataSize < 1) ||
        (pTxMessage->DataSize > 256))
    {
        error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
        error.Field = IEBUS_DATA_SIZE_FIELD;
        goto error;
    }

    // Make sure we are not busy or pending a message.
    if ((IEBUS_FRAME_STATE_IDLE != IEBusTxFrameState) ||
        (IEBusTxMessageIsPending))
    {
        error.Error = IEBUS_TX_IN_PROGRESS;
        error.Field = IEBusFieldType[IEBusTxFrameState];
        goto error;
    }

    // Copy the message into a transmission buffer.
    memcpy(&IEBusTxMessage, pTxMessage, sizeof(IEBusMessageType));

    // Update the master address field if needed.
    if (IEBusTxMessage.MasterAddress == IEBUS_MY_ADDR)
    {
        IEBusTxMessage.MasterAddress = IEBusMyAddress;
    }

    // "If the broadcast bit is 0, broadcast is indicated. If the broadcast bit is 1,
    // ordinary communication is indicated.
    // There are two types of broadcast: group broadcast and general broadcast.
    // These types are distinguished by the slave address."
    //
    // "For broadcast, the slave address is used to distinguish between group broadcast or
    // general broadcast, as follows:
    //      When the slave address is FFFH : General broadcast
    //      When the slave address is other than FFFH : Group broadcast
    //      For group broadcast, the number of a target group is indicated by the high-order
    //      4 bits of the slave address."
    if (IEBUS_BROADCAST_ALL == IEBusTxMessage.Broadcast)
    {
        IEBusTxMessage.SlaveAddress = IEBUS_BROADCAST_ADDR;
    }

    // Determine if this is a write or read message.
    IEBusTxMessageIsWrite = (IEBusTxMessage.Control & IEBUS_CONTROL_WRITE_MASK);

    // "In ordinary communication, transmission and reception are performed between one master unit
    // and one associated slave unit. Broadcast can also be done between one master unit and more
    // than one slave unit. In this case master unit transmits data to an arbitrary number of slave
    // units. In this case, the slave units do not return on acknowledge signal to the master unit."
    //
    // "The acknowledge bit is defined as follows:
    //   0: Indicates that transmission data has been recognized. (ACK)
    //   1: Indicates that no transmission data has been recognized. (NAK)"
    //
    // Determine the value for the Ack bit: if we are broacasting, send the
    // Rx ack bit since no slave will ack, otherwise send the Tx ack bit
    // so we can read the ack from the slave.
    IEBusTxAckBit = (IEBUS_BROADCAST_NONE == IEBusTxMessage.Broadcast) ?
        IEBUS_DATA_RX_ACK : IEBUS_DATA_TX_ACK;

    // Some of the enum values have extra bits set to allow for
    // essentially duplicate vaues, such as the control helpers
    // and the group vs. all broadcast. We'll leave this as-is in
    // the Tx message and allow the shifting based on bit-length
    // to truncate the extra bits.

    // Track original size.
    IEBusTxOriginalDataSize = IEBusTxMessage.DataSize;
    
    // Reset state.
    IEBusTxResetState();

    // Ready to transmit: set Tx frame state.
    IEBusTxMessageIsPending = TRUE;
    IEBusTxFrameState = IEBUS_FRAME_STATE_START_BIT;

error:

    // Notify error.
    if (IEBUS_ERROR_NONE != error.Error)
    {
        if (IEBusOnErrorCallback)
        {
            IEBusOnErrorCallback(&error);
        }

        return FALSE;
    }

    // Sucess.
    return TRUE;
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
void IEBusProcess(void)
{
    // When data is available, process it.
    if (IEBusDataAvailable)
    {
        // Process Tx state machine for both Tx and Rx bus states.
        // Process Rx first since IEBusTxProcessBit may leverage
        // data processed in IEBusRxProcessBit.
        if (IEBUS_BUS_STATE_IDLE != IEBusBusState)
        {
            IEBusRxProcessBit();
        }

        // Process Tx state machine
        if (IEBUS_BUS_STATE_TX == IEBusBusState)
        {
            IEBusTxProcessBit();
        }

        // Clrear data available flag
        IEBusDataAvailable = FALSE;
    }
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusTxResetState

  Description  :  Reset the state for Tx

  Argument(s)  :  None.

  Return value :  None.

--------------------------------------------------------------------------------------------------*/
void IEBusTxResetState(void)
{
    // Start from first data byte.
    IEBusTxDataFieldsSent = 0;
    IEBusTxDataFieldOffset = 0;
    
    // Reset parity and bit counts.
    IEBusTxParityBit = IEBUS_PARITY_DEFAULT;
    IEBusTxDataBitIndex = 0;
    IEBusTxDataTotalBitCount = 0;    
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusTxGetNextBit

  Description  :  Get the next bit to transmit.

  Argument(s)  :  None.

  Return value :  0 or 1.

  Notes        :  This is called after starting a bit for transmission to determine whether to transmit
                  a '1' or a '0' based on the Tx frame state and control. This function assumes it is being
                  called only in the transmit case.

                  This function adjusts IEBusTxParityBit, IEBusTxDataBitIndex,
                  IEBusTxDataTotalBitCount and IEBusTxData.  The next bit to transmit
                  is loaded into the high bit of IEBusTxData, i.e. IEBusTxData & 0x8000.

                  This function cannot be used to get the start bit.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusTxGetNextBit(void)
{
    // If the current field has not been completely sent,
    // grab the next bit of the current field.
    if (IEBusTxDataBitIndex < IEBusTxDataTotalBitCount)
    {
        // Track the bit count and shift the data.
        IEBusTxData <<= 1;
        ++IEBusTxDataBitIndex;

        // Track parity and return '1' or '0'.
        if (IEBusTxData & 0x8000)
        {
            IEBusTxParityBit = !IEBusTxParityBit;
            return 1;
        }

        return 0;
    }

    // Get the approriate field.
    switch (IEBusTxFrameState)
    {
        // Idle, nothing to transmit.
        // Start bit is a special case and we don't handle it here.
        case IEBUS_FRAME_STATE_IDLE:
        case IEBUS_FRAME_STATE_START_BIT:
            IEBusTxData = 0;
            IEBusTxDataBitIndex = 0;
            IEBusTxDataTotalBitCount = 0;
            IEBusTxParityBit = IEBUS_PARITY_DEFAULT;
            return 0;

        // "The header consists of a start bit and a broadcast bit."
        //
        // "The broadcast bit is used to distinguish between broadcast and ordinary communication.
        // If the broadcast bit is 0, broadcast is indicated. If the broadcast bit is 1, ordinary
        // communication is indicated."
        case IEBUS_FRAME_STATE_BROADCAST_FIELD:
            IEBusTxData = IEBusTxMessage.Broadcast;
            break;

        // "The master address field is used to transmit the local unit address (master address) to other units.
        // The master address field consists of master address bits and a parity bit.
        case IEBUS_FRAME_STATE_MASTER_ADDR_FIELD:
            IEBusTxData = IEBusTxMessage.MasterAddress;
            break;

        case IEBUS_FRAME_STATE_MASTER_ADDR_PARITY:
            IEBusTxData = IEBusTxParityBit;
            break;

        // "The slave address field is used to transmit the address (slave address) of a unit
        // (slave unit) with which the master unit wants to communicate.
        // The slave address field consists of slave address bits, a parity bit, and an acknowledge bit.
        case IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD:
            IEBusTxData = IEBusTxMessage.SlaveAddress;
            break;

        case IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY:
            IEBusTxData = IEBusTxParityBit;
            break;

        case IEBUS_FRAME_STATE_SLAVE_ADDR_ACK:
            IEBusTxData = IEBusTxAckBit;
            break;

        // "The control field indicates the type and direction of the next data field.
        // The control field consists of control bits, a parity bit, and an acknowledge bit.
        case IEBUS_FRAME_STATE_CONTROL_FIELD:
            IEBusTxData = IEBusTxMessage.Control;
            break;

        case IEBUS_FRAME_STATE_CONTROL_PARITY:
            IEBusTxData = IEBusTxParityBit;
            break;

        case IEBUS_FRAME_STATE_CONTROL_ACK:
            IEBusTxData = IEBusTxAckBit;
            break;

        // "The data-length field specifies the communication data length, in bytes."
        //
        // "The operation performed for this field differs depending on whether master transmission
        // (when bit 3 of the control bits is 1) or master reception (when bit 3 of the control bits is 0)
        // is performed."
        //
        // Master Transmission:
        // "The data-length bits and parity bit are output by the master unit. When the slave unit
        // detects even parity, the slave unit outputs an acknowledge signal, then proceeds to the
        // data field. For broadcast, however, the slave unit does not output an acknowledge signal.
        // If the slave unit detects odd parity, the slave unit does not output an acknowledge signal,
        // regarding the received data-length bits as being incorrect. Then, the slave unit enters
        // the standby (monitor) state again. At this time, the master unit also enters the standby
        // state again, and communication terminates."
        //
        // Master Reception:
        // "The data-length bits and parity bit are output by the slave unit. When the master unit detects
        // even parity, the master unit outputs the acknowledge signal.  if the master unit detects odd parity,
        // the master unit does not output an acknowledge signal, regarding the received data-length bits as
        // being incorrect. Then, the master unit enters the standby state again. At this time, the slave unit
        // also enters the standby state again, and communication terminates."
        case IEBUS_FRAME_STATE_DATA_SIZE_FIELD:
            IEBusTxData = (IEBusTxMessageIsWrite) ? IEBusTxMessage.DataSize : IEBUS_DATA_TX_READ_BYTE;
            break;

        case IEBUS_FRAME_STATE_DATA_SIZE_PARITY:
            IEBusTxData = (IEBusTxMessageIsWrite) ? IEBusTxParityBit : IEBUS_DATA_TX_READ_BIT;
            break;

        case IEBUS_FRAME_STATE_DATA_SIZE_ACK:
            IEBusTxData = (IEBusTxMessageIsWrite) ? IEBusTxAckBit : IEBUS_DATA_RX_ACK;
            break;

        // "The data field is used for data transmission and reception to and from a slave unit.
        // The master unit uses the data field to transmit data to the slave unit, or to receive
        // data from the slave unit."
        //
        // "The data field consists of data bits, a parity bit, and an acknowledge bit.
        // The eight data bits are output, starting with the MSB."
        //
        // "After the data bits have been output, the parity bit and acknowledge bit are output from
        // the master unit or slave unit, depending on on whether master transmission
        // (when bit 3 of the control bits is 1) or master reception (when bit 3 of the control bits is 0)
        // is performed."
        //
        // Master Transmission:
        // "When the master unit performs a write to a slave unit, the master unit transmits
        // the data bits and a parity bit to the slave unit. The slave unit receives the
        // data bits and parity bit, then outputs an acknowledge signal if even parity is
        // detected and the reception buffer is empty. If odd parity is detected, or if the
        // reception buffer is not empty, the slave unit rejects the corresponding data,
        // and does not output an acknowledge signal."
        //
        // Master Reception:
        // "When the master unit reads data from a slave unit, the master unit outputs a synchronization
        // signal for each bit as it is read. The slave unit outputs data and a parity bit to the bus
        // according to the synchronization signal output by the master unit. The master unit reads the
        // data and parity bit output by the slave unit, and checks the parity. If the master unit
        // detects odd parity, or if the reception buffer is not empty, the master unit rejects the data,
        // and does not output an acknowledge signal."
        case IEBUS_FRAME_STATE_DATA_FIELD:
            IEBusTxData = (IEBusTxMessageIsWrite) ? IEBusTxMessage.Data[IEBusTxDataFieldOffset] : IEBUS_DATA_TX_READ_BYTE;
            break;

        case IEBUS_FRAME_STATE_DATA_PARITY:
            IEBusTxData = (IEBusTxMessageIsWrite) ? IEBusTxParityBit : IEBUS_DATA_TX_READ_BIT;
            break;

        case IEBUS_FRAME_STATE_DATA_ACK:
            IEBusTxData = (IEBusTxMessageIsWrite) ? IEBusTxAckBit : IEBUS_DATA_RX_ACK;
            break;
    }

    // Reset parity and bit counts.
    IEBusTxParityBit = IEBUS_PARITY_DEFAULT;
    IEBusTxDataBitIndex = 0;
    IEBusTxDataTotalBitCount = IEBusExpectedFieldBitLength[IEBusTxFrameState];

    // Shift the data so that we send the most significant bit first.
    IEBusTxData <<= (16 - IEBusTxDataTotalBitCount);

    // Track parity and return '1' or '0'.
    if (IEBusTxData & 0x8000)
    {
        IEBusTxParityBit = !IEBusTxParityBit;
        return 1;
    }

    return 0;
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusTxProcessBit

  Description  :  Process the transmit state machine when a bit completes.

  Argument(s)  :  None

  Return value :  None

  Notes        :  This runs after a bit is received for transmission to
                  determine the next field to be transmitted.

--------------------------------------------------------------------------------------------------*/
void IEBusTxProcessBit(void)
{
    // Idle, nothing to do.
    if (IEBUS_FRAME_STATE_IDLE == IEBusTxFrameState)
    {
        return;
    }

    // If the current field has not been completely sent,
    // Default to no error
    IEBusErrorMessageType error = { IEBUS_ERROR_NONE };

    // If we received a start bit but were not transmitting one, someone else took
    // control of the bus as long as we were in a frame state after the start bit.
    if ((IEBUS_DATA_START_BIT == IEBusRxData) &&
        (IEBusTxFrameState > IEBUS_FRAME_STATE_START_BIT))
    {
        // Reset state.
        IEBusTxResetState();
        
        // Notify error.
        error.Error = IEBUS_ERROR_COLLISION;
        error.Field = IEBusFieldType[IEBusTxFrameState];

        // The bus is not idle so transition to receive and
        // set frame to resend the Tx message when bus becomes idle.
        IEBusTxMessageIsPending = TRUE;
        IEBusBusState = IEBUS_BUS_STATE_RX;

        goto error;
    }

    // Process the response from the current state
    uint8_t skipBitVerification = FALSE;
    switch (IEBusTxFrameState)
    {
        // Idle, nothing to do.
        case IEBUS_FRAME_STATE_IDLE:
            return;

        // "The header consists of a start bit and a broadcast bit."
        //
        // "The start bit is a signal used to notify the other units of the beginning of data transmission.
        // Before a unit starts data transmission, it outputs a low-level signal (start bit) for a
        // specified duration, then outputs the broadcast bit."
        //
        // "When the unit attempts to output the start bit, another unit may have already output
        // the start bit. In such a case, the unit does not output the start bit, and instead waits
        // for the other unit to stop outputting the start bit. Then, synchronized with the
        // completion of start bit output by the other unit, the unit starts output of the
        // broadcast bit."
        case IEBUS_FRAME_STATE_START_BIT:
            if (IEBusRxData != IEBUS_DATA_START_BIT)
            {
                // What we had hoped was a start bit was not in fact a start bit.
                // Therefore, the bus is not idle so transition to receive and
                // set frame to resend the Tx message when bus becomes idle.
                IEBusTxFrameState = IEBUS_FRAME_STATE_IDLE;
                IEBusTxMessageIsPending = TRUE;
                IEBusBusState = IEBUS_BUS_STATE_RX;
                
                // Jump to error to avoid incrementing IEBusTxFrameState.
                // But this is not an error case so do not set error.Error.
                goto error;
            }
            else
            {
                // Reset state.
                IEBusTxResetState();
                
                // If sending a start bit, message is no longer pending and
                // notify begining of transmit.
                IEBusTxMessageIsPending = FALSE;
                if (IEBusStateChangeCallback)
                {
                    IEBusStateChangeType state = { IEBUS_TX_BEGIN, &IEBusTxMessage };
                    IEBusStateChangeCallback(&state);
                }
            }
            
            skipBitVerification = TRUE;
            break;

        // "The broadcast bit is used to distinguish between broadcast and ordinary communication.
        // If the broadcast bit is 0, broadcast is indicated. If the broadcast bit is 1, ordinary
        // communication is indicated."
        //
        // "When more than one unit starts sending a communication frame at the same time,
        // broadcast takes precedence over ordinary communication and wins arbitration."
        case IEBUS_FRAME_STATE_BROADCAST_FIELD:
            break;

        // "The master address field is used to transmit the local unit address (master address) to other units.
        // The master address field consists of master address bits and a parity bit.
        // A master address consists of 12 bits. It is output starting with the MSB.
        // Next, the master unit outputs a parity bit to post the master address to other units."
        //
        // "Each time a unit transmits one bit of the master address field, the unit compares its
        // output data with the data on the bus. If the comparison indicates that the master address
        // output by the unit differs from the data on the bus, the unit determines that it has lost
        // an arbitration. The unit stops transmission, and readies itself for reception."
        //
        // "When arbitration is performed between units (arbitration masters), the
        // unit having the smallest master address value wins the arbitration.
        // After the 12-bit master address has been output, only one unit is finally determined as being
        // the master unit, such that that unit remains in the transmission state."
        case IEBUS_FRAME_STATE_MASTER_ADDR_FIELD:
            break;

        case IEBUS_FRAME_STATE_MASTER_ADDR_PARITY:
            break;

        // "The slave address field is used to transmit the address (slave address) of a unit
        // (slave unit) with which the master unit wants to communicate.
        // The slave address field consists of slave address bits, a parity bit, and an acknowledge bit.
        // A slave address consists of 12 bits. It is output starting with the MSB.
        // After a 12-bit slave address has been transmitted, a parity bit is output to prevent
        // the slave address from being received incorrectly. Then, the master unit attempts to detect
        // the acknowledge signal from a slave unit to confirm that the slave unit exists on the bus."
        //
        // "When the acknowledge signal is detected, the master unit outputs a control field. Note, however,
        // that when performing broadcast, the master unit outputs the control field without attempting to
        // detect the acknowledge bit."
        //
        // "If the broadcast bit is 0, broadcast is indicated. If the broadcast bit is 1,
        // ordinary communication is indicated.
        // There are two types of broadcast: group broadcast and general broadcast.
        // These types are distinguished by the slave address."
        //
        // "For broadcast, the slave address is used to distinguish between group broadcast or
        // general broadcast, as follows:
        //      When the slave address is FFFH : General broadcast
        //      When the slave address is other than FFFH : Group broadcast
        //      For group broadcast, the number of a target group is indicated by the high-order
        //      4 bits of the slave address."
        case IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD:
            break;

        case IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY:
            break;

        case IEBUS_FRAME_STATE_SLAVE_ADDR_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                // Notify error.
                error.Error = IEBUS_ERROR_UNACKNOWLEDGED_MSG;
                error.Field = IEBusFieldType[IEBusTxFrameState];
                goto error;
            }

            skipBitVerification = TRUE;
            break;

        // "The control field indicates the type and direction of the next data field.
        // The control field consists of control bits, a parity bit, and an acknowledge bit.
        // The four control bits are output starting with the MSB.
        // Following the control bits, a parity bit is output.
        //
        // "After detecting the acknowledge signal, the master unit proceeds to the data-length field.
        // If an acknowledge signal is not detected, the master unit enters the standby state,
        // terminating communication. For broadcast, however, the master unit proceeds to the
        // next data-length field without attempting to detect the acknowledge signal."
        case IEBUS_FRAME_STATE_CONTROL_FIELD:
            break;

        case IEBUS_FRAME_STATE_CONTROL_PARITY:
            break;

        case IEBUS_FRAME_STATE_CONTROL_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                // Notify error.
                error.Error = IEBUS_ERROR_UNACKNOWLEDGED_MSG;
                error.Field = IEBusFieldType[IEBusTxFrameState];
                goto error;
            }

            skipBitVerification = TRUE;
            break;

        // "The data-length field specifies the communication data length, in bytes."
        //
        // "The operation performed for this field differs depending on whether master transmission
        // (when bit 3 of the control bits is 1) or master reception (when bit 3 of the control bits is 0)
        // is performed."
        //
        // "The data-length bits and parity bit are output by the master unit. When the slave unit
        // detects even parity, the slave unit outputs an acknowledge signal, then proceeds to the
        // data field. For broadcast, however, the slave unit does not output an acknowledge signal.
        // If the slave unit detects odd parity, the slave unit does not output an acknowledge signal,
        // regarding the received data-length bits as being incorrect. Then, the slave unit enters
        // the standby (monitor) state again. At this time, the master unit also enters the standby
        // state again, and communication terminates."
        //
        // "If the data length set in the data-length bits exceeds the maximum number of transmission bytes, the
        // "latter varying with the communication mode, more than one frame is transmitted. In the second and
        // "subsequent frames, the data-length bits indicate the remaining communication data length, in bytes.
        //
        // Note: 0x00 is 256 bytes to transmit:
        //       Mode 0 max bytes/frame is 16
        //       Mode 1 max bytes/frame is 32
        case IEBUS_FRAME_STATE_DATA_SIZE_FIELD:
            break;

        case IEBUS_FRAME_STATE_DATA_SIZE_PARITY:
            if (!IEBusTxMessageIsWrite)
            {
                // For read messages, we need to copy the data size from Rx message to
                // Tx message variables so that we continue to kick-start the transmission
                // for all data bytes.  However, is this value is larger than the maximum
                // number of bytes per frame, I think it's unclear how the master is
                // supposed to continue the conversation.  We'll  consider this unsupported
                // and stop transmission.
                IEBusTxOriginalDataSize = IEBusRxMessage.DataSize;
                IEBusTxMessage.DataSize = IEBusRxMessage.DataSize;
                if (IEBusRxMessage.DataSize > IEBUS_MAX_DATA_LENGTH_BYTES)
                {
                    // Notify error.
                    error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
                    error.Field = IEBusFieldType[IEBusTxFrameState];
                    goto error;
                }

                skipBitVerification = TRUE;
            }
            break;

        case IEBUS_FRAME_STATE_DATA_SIZE_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                // Notify error.
                error.Error = IEBUS_ERROR_UNACKNOWLEDGED_MSG;
                error.Field = IEBusFieldType[IEBusTxFrameState];
                goto error;
            }

            skipBitVerification = TRUE;
            break;

        // "The data field is used for data transmission and reception to and from a slave unit.
        // The master unit uses the data field to transmit data to the slave unit, or to receive
        // data from the slave unit."
        //
        // "The data field consists of data bits, a parity bit, and an acknowledge bit.
        // The eight data bits are output, starting with the MSB."
        //
        // "After the data bits have been output, the parity bit and acknowledge bit are output from
        // the master unit or slave unit, depending on on whether master transmission
        // (when bit 3 of the control bits is 1) or master reception (when bit 3 of the control bits is 0)
        // is performed."
        //
        // "Broadcast is performed only when the master unit transmits data. At this time, any acknowledge
        // signal is ignored."
        //
        // "When the master unit performs a write to a slave unit, the master unit transmits
        // the data bits and a parity bit to the slave unit. The slave unit receives the
        // data bits and parity bit, then outputs an acknowledge signal if even parity is
        // detected and the reception buffer is empty. If odd parity is detected, or if the
        // reception buffer is not empty, the slave unit rejects the corresponding data,
        // and does not output an acknowledge signal."
        //
        // "If no acknowledge signal is received from the slave unit, the master unit transmits
        // the same data again. The master unit repeats this operation until it receives an
        // acknowledge signal from the slave unit,  or until the data exceeds the maximum number
        // of transmission bytes. When even parity is detected, and an acknowledge signal
        // is received from the slave unit, the master unit  transmits the subsequent data,
        // if any, and provided the maximum number of transmission bytes is not reached."
        //
        // "For broadcast, an acknowledge signal is not output by any slave unit. The master
        // unit transfers data one byte at a time."
        //
        // "If the data length set in the data-length bits exceeds the maximum number of transmission bytes, the
        // latter varying with the communication mode, more than one frame is transmitted. In the second and
        // subsequent frames, the data-length bits indicate the remaining communication data length, in bytes."
        case IEBUS_FRAME_STATE_DATA_FIELD:
            break;

        case IEBUS_FRAME_STATE_DATA_PARITY:
            if (!IEBusTxMessageIsWrite)
            {
                // For read messages, we need to copy the data size from Rx message to
                // Tx message variables so that we populate the Tx message with the result
                // form the slave.
                IEBusTxMessage.Data[IEBusTxDataFieldOffset] = IEBusRxMessage.Data[IEBusRxDataFieldOffset];

                skipBitVerification = TRUE;
            }
            ++IEBusTxDataFieldsSent;
            break;

        case IEBUS_FRAME_STATE_DATA_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                // "If any of the following is detected, the acknowledge bit at the end of the data field
                // is set to NAK, and transmission is stopped:
                //  - The parity of the data bits is incorrectNote.
                //  - A timing error occurred after the previous acknowledge bit.
                //  - The reception buffer is full, such that no more data can be accepted *Note*."
                //
                // "Note: In this case, if the maximum allowable number of transmission bytes per frame has
                // not yet been reached, the transmitter retries transmission of the data field until the
                // maximum number of transmission bytes is reached."
                //
                // Do not increment IEBusTxDataFieldOffset to resend data byte.

                // Provide an error notification but don't error out of the  sending process.
                // The spec does seem ambiguous in that only one condition of nack should allow
                // transmission to continue because there is no way for the sender to know which
                // of these conditions caused the slave to nack.  Let's just resend.
                if (IEBusOnErrorCallback)
                {
                    IEBusErrorMessageType error = { IEBUS_ERROR_RESENDING, IEBUS_DATA_FIELD, { IEBusTxDataFieldOffset } };
                    IEBusOnErrorCallback(&error);
                }
            }
            else
            {
                // Increment IEBusTxDataFieldOffset to send the next data byte.
                ++IEBusTxDataFieldOffset;
            }

            skipBitVerification = TRUE;

            // "If the data length set in the data-length bits exceeds the maximum number of transmission bytes, the
            // latter varying with the communication mode, more than one frame is transmitted. In the second and
            // subsequent frames, the data-length bits indicate the remaining communication data length, in bytes."
            //
            // "If the maximum allowable number of transmission bytes per frame has not yet been reached,
            // the transmitter retries transmission of the data field until the maximum number of transmission
            // bytes is reached."
            //
            if (IEBusTxDataFieldsSent >= IEBUS_MAX_DATA_LENGTH_BYTES)
            {
                // For read messages, we don't support data sizes beyond the maximum bytes per frame
                // but we could reach this point if reading a size near the maximum and parity
                // errors occur.  In this case, return an error.
                if (!IEBusTxMessageIsWrite)
                {
                    // Notify error.
                    error.Error = IEBUS_ERROR_UNSUPPORTED_MSG;
                    error.Field = IEBusFieldType[IEBusTxFrameState];
                    error.Data[0] = IEBusTxDataFieldsSent;
                    goto error;
                }

                // Subtract the number of bytes sent so far to yield the number
                // of bytes left to be sent, which will be transmitted in the next frame.
                // Do not reset IEBusTxDataFieldOffset so that we transmit the rest of the data.
                IEBusTxMessage.DataSize -= IEBusTxDataFieldOffset;

                // Track that we have another message to send.  The state will move ot idle at the
                // end of the function.
                IEBusTxMessageIsPending = TRUE;

                // Notify the completion of the frame but not the message.
                if (IEBusStateChangeCallback)
                {
                    IEBusStateChangeType state = { IEBUS_TX_PARTIAL, &IEBusTxMessage };
                    IEBusStateChangeCallback(&state);
                }
            }
            else if (IEBusTxDataFieldOffset < IEBusTxOriginalDataSize)
            {
                // Move the state machine to the state prior to the data field
                // in order to transmit another byte.  IEBusTxDataFieldOffset
                // tracks the index of the next byte to send.
                IEBusTxFrameState = IEBUS_FRAME_STATE_DATA_SIZE_ACK;
            }
            else
            {
                // Transmission of IEBusTxMessage is complete.  Restore the
                // data size to the original transmit message for notification.
                IEBusTxMessage.DataSize = IEBusTxOriginalDataSize;

                // Report status. For a read message, the Tx message now contains the result.
                if (IEBusStateChangeCallback)
                {
                    IEBusStateChangeType state = { IEBUS_TX_COMPLETE, &IEBusTxMessage };
                    IEBusStateChangeCallback(&state);
                }
            }
            break;
    }

    // verify the data we intended to send was sent.
    if ((FALSE == skipBitVerification) && (IEBusTxDataBitIndex <= IEBusTxDataTotalBitCount))
    {
        // During arbitration, a  broadcast transmission or lower-value
        // master address may also try transmitting and we don't want to continue
        // transmitting and corrupt the bus.
        //
        // We are transmitting MSB bit first so the TxData MSB holds
        // the last transmitted bit while the RxData LSB holds the most
        // recently receieved bit.
        // TODO: optimize this?
        if ((IEBusTxData & 0x8000) != (IEBusRxData << 15))
        {
            // Notify error.
            error.Error = IEBUS_ERROR_COLLISION;
            error.Field = IEBusFieldType[IEBusTxFrameState];

            // We lost arbitration so transition to receive and set frame
            // state to resend the Tx message when bus becomes idle.
            IEBusTxMessageIsPending = TRUE;
            IEBusTxFrameState = IEBUS_FRAME_STATE_IDLE;
            IEBusBusState = IEBUS_BUS_STATE_RX;

            goto error;
        }
    }

    // Move to the next frame state after processing the current field.
    ++IEBusTxFrameState;
    if (IEBusTxFrameState >= IEBUS_FRAME_MAX)
    {
        IEBusTxFrameState = IEBUS_FRAME_STATE_IDLE;
    }

error:

    // Handle and notify error.
    if (IEBUS_ERROR_NONE != error.Error)
    {
        // Notify error.
        if (IEBusOnErrorCallback)
        {
            IEBusOnErrorCallback(&error);
        }

        // Abandon transmission.
        IEBusTxFrameState = IEBUS_FRAME_STATE_IDLE;
        if (IEBUS_BUS_STATE_TX == IEBusBusState)
        {
            IEBusBusState = IEBUS_BUS_STATE_IDLE;
        }

        // Report status
        if (IEBusStateChangeCallback)
        {
            IEBusStateChangeType state = { IEBUS_TX_INCOMPLETE, &IEBusTxMessage };
            IEBusStateChangeCallback(&state);
        }
    }
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusRxResetState

  Description  :  Reset the state for Rx

  Argument(s)  :  None.

  Return value :  None.

--------------------------------------------------------------------------------------------------*/
void IEBusRxResetState(void)
{
    // Start from first data byte.
    IEBusRxDataFieldsSent = 0;
    IEBusRxDataFieldOffset = 0;
    
    // Reset parity and bit counts.
    IEBusRxParityBit = IEBUS_PARITY_DEFAULT;
    IEBusRxDataBitIndex = 0;
    IEBusRxDataTotalBitCount = 0;
    
    IEBusRxResponseParityBit = IEBUS_PARITY_DEFAULT;
    IEBusRxResponseDataBitIndex = 0;
    IEBusRxResponseDataTotalBitCount = 0;
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusRxShouldSendBit

  Description  :  Determine if a bit should be sent.

  Argument(s)  :  None

  Return value :  TRUE if a bit should be sent, FALSE if not.

  Notes        :  This function is called to determine if a bit needs to be sent based
                  on the Rx frame state and message contents.  This function assumes it is being
                  called only in the receive case.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusRxShouldSendBit(void)
{
    // Send ack as necessary.
    switch (IEBusRxFrameState)
    {
        // We ack data size and data fields when
        // a write message is addressed to us.
        case IEBUS_FRAME_STATE_DATA_SIZE_ACK:
        case IEBUS_FRAME_STATE_DATA_ACK:
            if (!IEBusRxMessageIsWrite)
            {
                return FALSE;
            }

            // Fallthrough: the next condition checks
            // if the message is addressed to us.

        // We ack slave address and control fields
        // when the message is addressed to us.
        case IEBUS_FRAME_STATE_SLAVE_ADDR_ACK:
        case IEBUS_FRAME_STATE_CONTROL_ACK:
            if (!IEBusRxMessageToMe)
            {
                return FALSE;
            }

            return TRUE;

        // We send data size/data and their parity bits
        // when we are sending a response
        case IEBUS_FRAME_STATE_DATA_SIZE_PARITY:
        case IEBUS_FRAME_STATE_DATA_PARITY:
        case IEBUS_FRAME_STATE_DATA_SIZE_FIELD:
        case IEBUS_FRAME_STATE_DATA_FIELD:
            if (IEBusRxMessageIsWrite || (!IEBusRxMessageToMe))
            {
                return FALSE;
            }

            return TRUE;

        // No responses for any other frame
        case IEBUS_FRAME_STATE_IDLE:
        case IEBUS_FRAME_STATE_START_BIT:
        case IEBUS_FRAME_STATE_BROADCAST_FIELD:
        case IEBUS_FRAME_STATE_MASTER_ADDR_FIELD:
        case IEBUS_FRAME_STATE_MASTER_ADDR_PARITY:
        case IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD:
        case IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY:
        case IEBUS_FRAME_STATE_CONTROL_FIELD:
        case IEBUS_FRAME_STATE_CONTROL_PARITY:
            break;
    }

    return FALSE;
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusRxGetNextBit

  Description  :  Get the next bit to transmit for response.

  Argument(s)  :  None.

  Return value :  0 or 1.

  Notes        :  This is called after starting a bit for transmission to determine whether to transmit
                  a '1' or a '0' based on the Rx frame state and message contents.  Transmission occurs
                  during Rx to send acks and responses to read messages.

                  This function should only be called when IEBusRxShouldSendBit() returns true.

                  This function adjusts IEBusRxResponseParityBit and IEBusRxResponseData.
                  The next bit to transmit is loaded into the high bit of IEBusRxResponseData,
                  i.e. IEBusRxResponseData & 0x80.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusRxGetNextBit(void)
{
    // "The data-length field specifies the communication data length, in bytes."
    //
    // "The operation performed for this field differs depending on whether master
    // transmission (when bit 3 of the control bits is 1) or master reception
    // (when bit 3 of the control bits is 0) is performed."
    //
    // "The data field consists of data bits, a parity bit, and an acknowledge bit.
    // The eight data bits are output, starting with the MSB."
    //
    // Master Reception:
    // "The data-length bits and parity bit are output by the slave unit.
    // When the master unit detects even parity,
    // the master unit outputs the acknowledge signal.
    // If the master unit detects odd parity, the master unit does not
    // output an acknowledge signal, regarding the
    // received data-length bits as being incorrect. At this
    // time, the slave unit enters the standby state again, and communication terminates."
    //
    switch (IEBusRxFrameState)
    {
        // Send a ack bit.
        case IEBUS_FRAME_STATE_SLAVE_ADDR_ACK:
        case IEBUS_FRAME_STATE_CONTROL_ACK:
        case IEBUS_FRAME_STATE_DATA_SIZE_ACK:
        case IEBUS_FRAME_STATE_DATA_ACK:
            return IEBUS_DATA_RX_ACK;

        // Master Reception:
        // "The data-length bits and parity bit are output by the slave unit.
        // When the master unit detects even parity,
        // the master unit outputs the acknowledge signal.
        // Send data size/data parity bit.
        case IEBUS_FRAME_STATE_DATA_SIZE_PARITY:
        case IEBUS_FRAME_STATE_DATA_PARITY:
            return IEBusRxResponseParityBit;

        // Master Reception:
        // "The data-length bits and parity bit are output by the slave unit.
        // When the master unit detects even parity,
        // the master unit outputs the acknowledge signal.
        // Send data size/data bit when we are
        // sending a response.
        case IEBUS_FRAME_STATE_DATA_SIZE_FIELD:
        case IEBUS_FRAME_STATE_DATA_FIELD:

            // If the current field has not been completely sent,
            // grab the next bit of the current field.
            // In Rx read mode, all bytes are 8 bits.
            if (IEBusRxResponseDataBitIndex < IEBusRxResponseDataTotalBitCount)
            {
                // Track the bit count and shift the data.
                IEBusRxResponseData <<= 1;
                ++IEBusRxResponseDataBitIndex;

                // Track parity and return '1' or '0'.
                if (IEBusRxResponseData & 0x80)
                {
                    IEBusRxResponseParityBit = !IEBusRxResponseParityBit;
                    return 1;
                }

                return 0;
            }

            // Get data byte.  IEBusRxResponseData is 8-bits wide so unlike
            // Tx which may send 12 bits, there is no need for shifting
            // the data as MSB of the data is already in MSB in IEBusRxResponseData.
            // IEBusRxDataFieldOffset is already managed for the reception of data bits
            // and can be used to transmit the response as well.
            IEBusRxResponseData = (IEBUS_FRAME_STATE_DATA_SIZE_FIELD == IEBusRxFrameState) ?
                                    IEBusRxMessage.DataSize :
                                    IEBusRxMessage.Data[IEBusRxDataFieldOffset];

            // Reset parity and bit counts.
            IEBusRxResponseParityBit = IEBUS_PARITY_DEFAULT;
            IEBusRxResponseDataBitIndex = 0;
            IEBusRxResponseDataTotalBitCount = (IEBUS_FRAME_STATE_DATA_SIZE_FIELD == IEBusRxFrameState) ?
                                                 IEBUS_DATA_SIZE_FIELD_LENGTH : IEBUS_DATA_FIELD_LENGTH;

            // Track parity and return '1' or '0'.
            if (IEBusRxResponseData & 0x80)
            {
                IEBusRxResponseParityBit = !IEBusRxResponseParityBit;
                return 1;
            }

            return 0;

        // No responses for any other frame
        case IEBUS_FRAME_STATE_IDLE:
        case IEBUS_FRAME_STATE_START_BIT:
        case IEBUS_FRAME_STATE_BROADCAST_FIELD:
        case IEBUS_FRAME_STATE_MASTER_ADDR_FIELD:
        case IEBUS_FRAME_STATE_MASTER_ADDR_PARITY:
        case IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD:
        case IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY:
        case IEBUS_FRAME_STATE_CONTROL_FIELD:
        case IEBUS_FRAME_STATE_CONTROL_PARITY:
            break;
    }

    // Otherwise, return a "nack".  This is a '1' which is the saftest data to put
    // on the data bus so it is the best value for a case we should never reach.
    return IEBUS_DATA_RX_NACK;
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusRxProcessBit

  Description  :  Process the receive state machine when a bit completes.

  Argument(s)  :  None

  Return value :  None

  Notes        :  This runs after a bit is received for reception to
                  process the field.

--------------------------------------------------------------------------------------------------*/
void IEBusRxProcessBit(void)
{
    // Idle, nothing to do.
    if (IEBUS_FRAME_STATE_IDLE == IEBusRxFrameState)
    {
        return;
    }

    // Default to no error
    IEBusErrorMessageType error = { IEBUS_ERROR_NONE };

    // If the current field has not been completely received,
    // track the next bit of the current field.
    if (IEBusRxDataBitIndex < IEBusRxDataTotalBitCount)
    {
        // Track even parity: check the most recent bit for a '1' and
        // track even parity as required.
        if (IEBusRxData & 0x01)
        {
            IEBusRxParityBit = !IEBusRxParityBit;
        }

        // Keep track of bits received and return.
        ++IEBusRxDataBitIndex;
        return;
    }

    // If we received a start bit but were not expecting one, someone else took
    // control of the bus as long as we were in a frame state after the start bit.
    if ((IEBUS_DATA_START_BIT == IEBusRxData) &&
        (IEBusRxFrameState > IEBUS_FRAME_STATE_START_BIT))
    {
        // Notify collision error.
        if (IEBusOnErrorCallback)
        {
            IEBusErrorMessageType collisionError = { IEBUS_ERROR_COLLISION, IEBusFieldType[IEBusRxFrameState], { IEBusRxDataFieldOffset } };
            IEBusOnErrorCallback(&collisionError);
        }

        // Report status of previous Rx frame.
        if (IEBusStateChangeCallback)
        {
            IEBusStateChangeType state = { IEBUS_RX_INCOMPLETE, &IEBusRxMessage };
            IEBusStateChangeCallback(&state);
        }

        // Set frame to start receiving a new message.
        // No need to send an error here, just continue below to
        // the start bit frame state.
        IEBusRxFrameState = IEBUS_FRAME_STATE_START_BIT;
    }

    // If we received a timeout during reception, reset frame state
    // and notify the error.
    if (IEBUS_DATA_TIMEOUT_BIT == IEBusRxData)
    {
        error.Error = IEBUS_ERROR_TIMEOUT;
        error.Field = IEBusFieldType[IEBusRxFrameState];
        error.Data[0] = IEBusRxDataFieldOffset;
        goto error;
    }

    // Process the response from the current state
    switch (IEBusRxFrameState)
    {
        // Idle, nothing to do.
        case IEBUS_FRAME_STATE_IDLE:
            return;

        // "The header consists of a start bit and a broadcast bit."
        //
        // "The start bit is a signal used to notify the other units of the beginning of data transmission.
        // Before a unit starts data transmission, it outputs a low-level signal (start bit) for a
        // specified duration, then outputs the broadcast bit."
        //
        // "All units, except that unit which started the transmission, detect the start bit and become
        // ready for reception."
        case IEBUS_FRAME_STATE_START_BIT:
            if (IEBusRxData != IEBUS_DATA_START_BIT)
            {
                // What we had hoped was a start bit was not in fact a start bit.
                // We don't have much hope of receiving this message so we'll wait
                // for the next start bit.
                IEBusRxFrameState = IEBUS_FRAME_STATE_IDLE;
                IEBusBusState = IEBUS_BUS_STATE_IDLE;
                
                // Jump to error to avoid incrementing IEBusRxFrameState.
                // But this is not an error case so do not set error.Error.
                goto error;
            }
            else
            {
                // Reset State
                IEBusRxResetState();
                
                // Notify.
                if (IEBusStateChangeCallback)
                {
                    IEBusStateChangeType state = { IEBUS_RX_BEGIN, 0 };
                    IEBusStateChangeCallback(&state);
                }
            }
            break;

        // "The broadcast bit is used to distinguish between broadcast and ordinary communication.
        // If the broadcast bit is 0, broadcast is indicated. If the broadcast bit is 1, ordinary
        // communication is indicated."
        case IEBUS_FRAME_STATE_BROADCAST_FIELD:
            IEBusRxMessage.Broadcast = IEBusRxData;
            break;

        // "The master address field is used to transmit the local unit address (master address) to other units.
        // The master address field consists of master address bits and a parity bit.
        // A master address consists of 12 bits. It is output starting with the MSB.
        // Next, the master unit outputs a parity bit to post the master address to other units."
        //
        case IEBUS_FRAME_STATE_MASTER_ADDR_FIELD:
            IEBusRxMessage.MasterAddress = IEBusRxData;
            IEBusRxMessageFromMe = (IEBusRxMessage.MasterAddress == IEBusMyAddress);
            break;

        case IEBUS_FRAME_STATE_MASTER_ADDR_PARITY:
            if (IEBusRxData != IEBusRxParityBit)
            {
                error.Error = IEBUS_ERROR_PARITY;
                error.Field = IEBUS_MASTER_ADDR_FIELD;
                goto error;
            }
            break;

        // "The slave address field is used to transmit the address (slave address) of a unit
        // (slave unit) with which the master unit wants to communicate.
        // The slave address field consists of slave address bits, a parity bit, and an acknowledge bit.
        // A slave address consists of 12 bits. It is output starting with the MSB.
        // After a 12-bit slave address has been transmitted, a parity bit is output to prevent
        // the slave address from being received incorrectly. Then, the master unit attempts to detect
        // the acknowledge signal from a slave unit to confirm that the slave unit exists on the bus."
        //
        // "The slave unit outputs an acknowledge signal when the slave unit recognizes a match
        // between the slave unit�s address and the slave address transmitted by the master unit match,
        // and that both the master address and slave address have even parity. If the slave unit
        // detects odd parity, it does not recognize the addresses as matching, so does not output
        // an acknowledge signal."
        //
        // "If the broadcast bit is 0, broadcast is indicated. If the broadcast bit is 1,
        // ordinary communication is indicated.
        // There are two types of broadcast: group broadcast and general broadcast.
        // These types are distinguished by the slave address."
        //
        // "For broadcast, the slave address is used to distinguish between group broadcast or
        // general broadcast, as follows:
        //      When the slave address is FFFH : General broadcast
        //      When the slave address is other than FFFH : Group broadcast
        //      For group broadcast, the number of a target group is indicated by the high-order
        //      4 bits of the slave address."
        case IEBUS_FRAME_STATE_SLAVE_ADDR_FIELD:
            IEBusRxMessage.SlaveAddress = IEBusRxData;

            // Determine if this message is for us and if it needs a response.
            IEBusRxForMe = FALSE;
            IEBusRxMessageToMe = FALSE;
            if (IEBusRxMessage.Broadcast)
            {
                if (IEBUS_BROADCAST_ADDR == IEBusRxMessage.SlaveAddress)
                {
                    IEBusRxForMe = TRUE;
                }
                if ((IEBusMyAddress & IEBUS_GROUP_BROADCAST_MASK) == (IEBusRxMessage.SlaveAddress & IEBUS_GROUP_BROADCAST_MASK))
                {
                    IEBusRxForMe = TRUE;
                }
            }
            else if (IEBusMyAddress == IEBusRxMessage.SlaveAddress)
            {
                IEBusRxForMe = TRUE;
                IEBusRxMessageToMe = TRUE;
            }
            break;

        case IEBUS_FRAME_STATE_SLAVE_ADDR_PARITY:
            if (IEBusRxData != IEBusRxParityBit)
            {
                error.Error = IEBUS_ERROR_PARITY;
                error.Field = IEBUS_SLAVE_ADDR_FIELD;
                goto error;
            }
            break;

        case IEBUS_FRAME_STATE_SLAVE_ADDR_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                error.Error = IEBUS_ERROR_UNACKNOWLEDGED_MSG;
                error.Field = IEBUS_SLAVE_ADDR_FIELD;
                goto error;
            }
            break;

        // "The control field indicates the type and direction of the next data field.
        // The control field consists of control bits, a parity bit, and an acknowledge bit.
        // The four control bits are output starting with the MSB.
        // Following the control bits, a parity bit is output. If even parity is detected,
        // and the function requested by the master unit can be performed by the slave unit,
        // the slave unit outputs an acknowledge signal. Then, the slave unit proceeds to the
        // data-length field. If the slave unit cannot perform the processing requested by the
        // master unit, even when even parity is detected, or if odd parity is detected, the
        // slave unit does not output an acknowledge signal, and it enters the standby (monitor)
        // state again."
        case IEBUS_FRAME_STATE_CONTROL_FIELD:
            IEBusRxMessage.Control = IEBusRxData;

            // Determine if the command should be processed given locking state.
            uint8_t unlockedOrFromLockingMaster = ((!IEBusLockStatus) ||
                                                   (IEBusLockAddress == IEBusRxMessage.MasterAddress));

            // Pre-populate error structure to indicate control problem
            error.Field = IEBUS_CONTROL_FIELD;
            error.Data[0] = IEBusRxMessage.Control;

            // determine locking.
            IEBusRxWillLock = FALSE;
            IEBusRxWillUnlock = FALSE;

            // Write commands
            IEBusRxMessageIsWrite = (IEBusRxMessage.Control & IEBUS_CONTROL_WRITE_MASK);
            if (IEBusRxMessageIsWrite)
            {
                // Handle write commands: see if we can process/ack
                //
                // "Once a unit has been locked by a master unit, the locked unit rejects the
                // control bits received from other than the master unit that requested the lock,
                // unless the value of the control bits is one of the values listed in Table 2-4. Then,
                // the unit does not output the acknowledge bit."
                //
                // Table 2-4 contains only read commands.
                //
                // Note: it's unclear if a broacast message can be accepted when locked
                //       but we'll disallow it; only allow the locking master addr when locked.
                if ((!unlockedOrFromLockingMaster) && (IEBusRxMessageToMe))
                {
                    // we are locked, do not send ack and stop reception.
                    error.Error = IEBUS_ERROR_LOCKED;
                    goto error;
                }

                // Check for valid write command.  All valid write commands
                // have the 0x08 and 0x02 bit set (0x0A):
                //
                // From Table 2-3:
                // Hex  Bits  Meaning
                // 8H 1 0 0 0 Undefined
                // 9H 1 0 0 1 Undefined
                // AH 1 0 1 0 Write command and locking
                // BH 1 0 1 1 Write data and locking
                // CH 1 1 0 0 Undefined
                // DH 1 1 0 1 Undefined
                // EH 1 1 1 0 Write command
                // FH 1 1 1 1 Write data
                if (IEBUS_CONTROL_WRITE_VALID_MASK != (IEBusRxMessage.Control & IEBUS_CONTROL_WRITE_VALID_MASK))
                {
                    error.Error = IEBUS_ERROR_INVALID_CONTROL;
                    goto error;
                }

                // "The master unit can lock the slave unit by specifying the lock with the
                // corresponding control bits (3H, AH, BH). In this case, when the transmission
                // or reception of acknowledge bit 0 for the data-length field has been
                // completed, but the communication frame is then terminated before transmission
                // or reception of as many data bytes as are specified by the data-length bits
                // is completed, the slave unit is locked. At this time, the bit
                // indicating the lock status (bit 2) in the slave status byte is set to 1."
                //
                // "For broadcast, locking or unlocking is not performed."
                //
                if ((IEBUS_CONTROL_WRITE_CMD_LOCK == IEBusRxMessage.Control) ||
                    (IEBUS_CONTROL_WRITE_DATA_LOCK == IEBusRxMessage.Control))
                {
                    IEBusRxWillLock = (unlockedOrFromLockingMaster && IEBusRxMessageToMe);
                }
            }

            // Broadcast message for a read command.
            else if (IEBusRxMessage.Broadcast)
            {
                // Read commands in broadcast mode make no sense.
                error.Error = IEBUS_ERROR_INVALID_CONTROL;
                error.Field = IEBUS_BROADCAST_FIELD;
                goto error;
            }

            // Read Commands addressed to me.
            else if (IEBusRxMessageToMe)
            {
                // Read data.
                if ((IEBUS_CONTROL_READ_DATA_LOCK == IEBusRxMessage.Control) ||
                    (IEBUS_CONTROL_READ_DATA_NO_LOCK == IEBusRxMessage.Control))
                {
                    // "Once a unit has been locked by a master unit, the locked unit rejects the
                    // control bits received from other than the master unit that requested the lock,
                    // unless the value of the control bits is one of the values listed in Table 2-4. Then,
                    // the unit does not output the acknowledge bit."
                    //
                    // If not unlocked or sent from the locking master, we cannot respond.
                    if (!unlockedOrFromLockingMaster)
                    {
                        error.Error = IEBUS_ERROR_LOCKED;
                        goto error;
                    }

                    // If no callback, we can't do read so do not send ack.
                    // If the client cannot process this request, do not send ack.
                    uint8_t sendAck = FALSE;
                    if (IEBusRxDataCallback)
                    {
                        // No need to set error in this case: either there is no callback
                        // or the read failed, in which case client already knows.
                        sendAck = IEBusRxDataCallback(&IEBusRxMessage);
                    }
                    if (!sendAck)
                    {
                        error.Error = IEBUS_ERROR_READ_FAILED;
                        goto error;
                    }

                    // "The master unit can lock the slave unit by specifying the lock with the
                    // corresponding control bits (3H, AH, BH). In this case, when the transmission
                    // or reception of acknowledge bit 0 for the data-length field has been
                    // completed, but the communication frame is then terminated before transmission
                    // or reception of as many data bytes as are specified by the data-length bits
                    // is completed, the slave unit is locked. At this time, the bit
                    // indicating the lock status (bit 2) in the slave status byte is set to 1."
                    //
                    // "For broadcast, locking or unlocking is not performed."
                    //
                    if (IEBUS_CONTROL_READ_DATA_LOCK == IEBusRxMessage.Control)
                    {
                        IEBusRxWillLock = unlockedOrFromLockingMaster;
                    }
                }

                // Read status
                else if ((IEBUS_CONTROL_READ_STATUS == IEBusRxMessage.Control) ||
                         (IEBUS_CONTROL_READ_STATUS_UNLOCK == IEBusRxMessage.Control))
                {
                    // "Once a unit has been locked by a master unit, the locked unit rejects the
                    // control bits received from other than the master unit that requested the lock,
                    // unless the value of the control bits is one of the values listed in Table 2-4. Then,
                    // the unit does not output the acknowledge bit."
                    //
                    // Table 2-4:
                    // Hex  Bits  Meaning
                    // 0H 0 0 0 0 Read slave status
                    // 4H 0 1 0 0 Read lock address (low-order 8 bits)
                    // 5H 0 1 0 1 Read lock address (high-order 4 bits)
                    if (!(unlockedOrFromLockingMaster || IEBUS_CONTROL_READ_STATUS == IEBusRxMessage.Control))
                    {
                        error.Error = IEBUS_ERROR_LOCKED;
                    }

                    // "A master unit can read the slave status (0H, 6H) to determine why the
                    // slave unit did not return the acknowledge bit (ACK)."
                    //
                    // "The slave status is determined from the result of the communication last
                    // performed by the slave unit. All slave units can provide slave status information."
                    //
                    // MSB                                         LSB
                    // bit 7 bit 6 bit 5 bit 4 bit 3 bit 2 bit 1 bit 0
                    //
                    // Bit Value Meaning
                    // Bit 0    0 The slave transmission buffer is empty.
                    //          1 The slave transmission buffer is not empty.
                    // Bit 1    0 The slave reception buffer is empty.
                    //          1 The slave reception buffer is not empty.
                    // Bit 2    0 The unit is not locked.
                    //          1 The unit is locked.
                    // Bit 3    0 Fixed at 0
                    // Bit 4    0 Slave transmission disabled
                    //          1 Slave transmission enabled
                    // Bit 5    0 Fixed at 0
                    // Bit 7    00 Mode 0
                    // Bit 6    01 Mode 1
                    //          10 Reserved for future expansion
                    //
                    // We don't implement transmit buffering
                    // We don't implement receive buffering
                    // Slave transmission is never disabled
                    //
                    // "Bits 7 and 6 are currently fixed to 10 in the hardware of the �PD72042B."
                    //
                    IEBusRxMessage.DataSize = 1;
                    IEBusRxMessage.Data[0] = ((IEBusLockStatus << IEBUS_STATUS_LOCK_SHIFT) & IEBUS_STATUS_LOCK_MASK) |
                                             (IEBUS_FIXED_STATUS_MODE);

                    // "The master unit can unlock a locked slave unit when the control bits
                    // specify locking (3H, AH, or BH) or unlocking (6H). The slave unit is
                    // unlocked once as many data bytes as are specified by the data-length bits
                    // have been transmitted or received within one communication frame. At this
                    // time, the bit indicating the lock status (bit 2) in the slave status byte
                    // is reset to 0."
                    //
                    // "For broadcast, locking or unlocking is not performed."
                    //
                    if (IEBUS_CONTROL_READ_STATUS_UNLOCK == IEBusRxMessage.Control)
                    {
                        IEBusRxWillUnlock = unlockedOrFromLockingMaster;
                    }
                }

                // Read lock address (low)
                else if (IEBUS_CONTROL_READ_LOCK_ADDR_LO == IEBusRxMessage.Control)
                {
                    // "When a lock address read operation (4H, 5H) is specified, the address
                    // (12 bits) of the master unit that issued the lock instruction is read
                    // in one-byte form, as shown below:
                    //
                    //                         MSB          LSB
                    // Control bits : 4H       Low-order 8 bits
                    //
                    // No mention of what should happen when master reads lock address and slave
                    // is not locked.  We might have an invalid address here.
                    //
                    IEBusRxMessage.DataSize = 1;
                    IEBusRxMessage.Data[0] = IEBusLockAddress & 0x00ff;
                }

                // Read lock address (hi)
                else if (IEBUS_CONTROL_READ_LOCK_ADDR_HI == IEBusRxMessage.Control)
                {
                    // "When a lock address read operation (4H, 5H) is specified, the address
                    // (12 bits) of the master unit that issued the lock instruction is read
                    // in one-byte form, as shown below:"
                    //
                    //                         MSB                       LSB
                    // Control bits : 5H       Undefined   High-order 4 bits
                    //
                    // No mention of what should happen when master reads lock address and slave
                    // is not locked.  We might have an invalid address here.
                    //
                    IEBusRxMessage.DataSize = 1;
                    IEBusRxMessage.Data[0] = IEBusLockAddress >> 8;
                }
                else
                {
                    // Unknown read command.
                    error.Error = IEBUS_ERROR_INVALID_CONTROL;
                    goto error;
                }
            }
            break;

        case IEBUS_FRAME_STATE_CONTROL_PARITY:
            if (IEBusRxData != IEBusRxParityBit)
            {
                error.Error = IEBUS_ERROR_PARITY;
                error.Field = IEBUS_CONTROL_FIELD;
                goto error;
            }
            break;

        case IEBUS_FRAME_STATE_CONTROL_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                error.Error = IEBUS_ERROR_UNACKNOWLEDGED_MSG;
                error.Field = IEBUS_CONTROL_FIELD;
                goto error;
            }
            break;

        // "The operation performed for this field differs depending on whether master transmission
        // (when bit 3 of the control bits is 1) or master reception (when bit 3 of the control bits is 0)
        // is performed."
        //
        // "The data-length field specifies the communication data length, in bytes.
        // The data-length field consists of the data-length bits, a parity bit, and an acknowledge bit.
        // The eight data-length bits are output starting with the MSB. The data-length bits
        // indicate the communication data length, in bytes as shown in Table 2-2."
        //
        // Data-length bit (hexadecimal) Transmission data length, in bytes
        // 01H      1
        // 02H      2
        // : :
        // : :
        // FFH      255
        // 00H      256
        //
        // "If the data length set in the data-length bits exceeds the maximum number of transmission bytes, the
        // "latter varying with the communication mode, more than one frame is transmitted. In the second and
        // "subsequent frames, the data-length bits indicate the remaining communication data length, in bytes.
        //
        // Save data size and reset Rx bytes received and data index.
        case IEBUS_FRAME_STATE_DATA_SIZE_FIELD:
            IEBusRxMessage.DataSize = IEBusRxData;
            IEBusRxDataFieldsSent = 0;
            IEBusRxDataFieldOffset = 0;
            break;

        case IEBUS_FRAME_STATE_DATA_SIZE_PARITY:
            if (IEBusRxData != IEBusRxParityBit)
            {
                error.Error = IEBUS_ERROR_PARITY;
                error.Field = IEBUS_DATA_SIZE_FIELD;
                goto error;
            }
            break;

        case IEBUS_FRAME_STATE_DATA_SIZE_ACK:
            if (IEBusRxData != IEBUS_DATA_RX_ACK)
            {
                error.Error = IEBUS_ERROR_UNACKNOWLEDGED_MSG;
                error.Field = IEBUS_DATA_SIZE_FIELD;
                goto error;
            }

            // Locking occurs after data size is sent and acked.
            if (IEBusRxWillLock)
            {
                IEBusLockAddress = IEBusRxMessage.MasterAddress;
                IEBusLockStatus = TRUE;

            }
            break;

        // "The data field is used for data transmission and reception to and from a slave unit.
        // The master unit uses the data field to transmit data to the slave unit, or to receive
        // data from the slave unit."
        //
        // "The data field consists of data bits, a parity bit, and an acknowledge bit.
        // The eight data bits are output, starting with the MSB."
        //
        // "After the data bits have been output, the parity bit and acknowledge bit are output from
        // the master unit or slave unit, depending on on whether master transmission
        // (when bit 3 of the control bits is 1) or master reception (when bit 3 of the control bits is 0)
        // is performed."
        //
        // "Broadcast is performed only when the master unit transmits data. At this time, any acknowledge
        // signal is ignored."
        //
        // "When the master unit performs a write to a slave unit, the master unit transmits
        // the data bits and a parity bit to the slave unit. The slave unit receives the
        // data bits and parity bit, then outputs an acknowledge signal if even parity is
        // detected and the reception buffer is empty. If odd parity is detected, or if the
        // reception buffer is not empty, the slave unit rejects the corresponding data,
        // and does not output an acknowledge signal."
        //
        // "If no acknowledge signal is received from the slave unit, the master unit transmits
        // the same data again. The master unit repeats this operation until it receives an
        // acknowledge signal from the slave unit,  or until the data exceeds the maximum number
        // of transmission bytes. When even parity is detected, and an acknowledge signal
        // is received from the slave unit, the master unit  transmits the subsequent data,
        // if any, and provided the maximum number of transmission bytes is not reached."
        //
        // "For broadcast, an acknowledge signal is not output by any slave unit. The master
        // unit transfers data one byte at a time."
        //
        // "If the data length set in the data-length bits exceeds the maximum number of transmission bytes, the
        // "latter varying with the communication mode, more than one frame is transmitted. In the second and
        // "subsequent frames, the data-length bits indicate the remaining communication data length, in bytes.
        //
        case IEBUS_FRAME_STATE_DATA_FIELD:
            IEBusRxMessage.Data[IEBusRxDataFieldOffset] = IEBusRxData;
            break;

        case IEBUS_FRAME_STATE_DATA_PARITY:
            if (IEBusRxData != IEBusRxParityBit)
            {
                error.Error = IEBUS_ERROR_PARITY;
                error.Field = IEBUS_DATA_FIELD;
                error.Data[0] = IEBusRxDataFieldOffset;
                goto error;
            }
            ++IEBusRxDataFieldsSent;
            break;

        case IEBUS_FRAME_STATE_DATA_ACK:
            // Master Transmission:
            // "When the master unit performs a write to a slave unit, the master unit
            // transmits the data bits and a parity bit to the slave unit. The slave unit
            // receives the data bits and parity bit, then outputs an acknowledge signal
            // if even parity is detected and the reception buffer is empty. If odd parity
            // is detected, or if the reception buffer is not empty, the slave unit rejects
            // the corresponding data, and does not output an acknowledge signal.
            // If no acknowledge signal is received from the slave unit, the master unit
            // transmits the same data again. The master unit repeats this operation until
            // it receives an acknowledge signal from the slave unit, or until the data exceeds
            // the maximum number of transmission bytes."
            //
            // Master Reception:
            // "If the master unit confirms even parity, and the reception buffer is
            // empty, the master unit accepts the data, and returns an acknowledge
            // signal to the slave unit. Then, the master unit reads the next data, provided
            // the maximum allowable number of transmission bytes per frame has not been reached."
            //
            // IEBusRxDataFieldOffset is used for both receving and transmiting a response in Rx mode
            // so checking the ack and adjusting IEBusRxDataFieldOffset handles both cases.
            if (IEBusRxData == IEBUS_DATA_RX_ACK)
            {
                ++IEBusRxDataFieldOffset;
            }

            // "If the data length set in the data-length bits exceeds the maximum number of transmission bytes, the
            // "latter varying with the communication mode, more than one frame is transmitted. In the second and
            // "subsequent frames, the data-length bits indicate the remaining communication data length, in bytes."
            //
            // "If the maximum allowable number of transmission bytes per frame has not yet been reached,
            // the transmitter retries transmission of the data field until the maximum number of transmission
            // bytes is reached.            "
            //
            if (IEBusRxDataFieldsSent >= IEBUS_MAX_DATA_LENGTH_BYTES)
            {
                // Allow client to process the write message if addressed
                // to us.
                if (IEBusRxForMe && IEBusDataWriteCallback)
                {
                    IEBusDataWriteCallback(&IEBusRxMessage);
                }

                // Notify the completion of the frame but not the message.
                if (IEBusStateChangeCallback)
                {
                    IEBusStateChangeType state = { IEBUS_RX_PARTIAL, &IEBusRxMessage };
                    IEBusStateChangeCallback(&state);
                }
            }
            else if (IEBusRxDataFieldOffset < IEBusRxMessage.DataSize)
            {
                // Move the state machine to the state prior to the data field
                // in order to receive another byte.  IEBusRxDataFieldOffset
                // tracks the index of the next byte to receive.
                IEBusRxFrameState = IEBUS_FRAME_STATE_DATA_SIZE_ACK;
            }
            else
            {
                // Unlocking occurs after data is sent and acked.
                if (IEBusRxWillUnlock)
                {
                    IEBusLockAddress = IEBUS_INVALID_ADDR;
                    IEBusLockStatus = FALSE;
    
                }
            
                // Allow client to process the write message if addressed
                // to us.
                if (IEBusRxForMe && IEBusDataWriteCallback)
                {
                    IEBusDataWriteCallback(&IEBusRxMessage);
                }

                // Reception of this data is complete.  Report status.
                if (IEBusStateChangeCallback)
                {
                    IEBusStateChangeType state = { IEBUS_RX_COMPLETE, &IEBusRxMessage };
                    IEBusStateChangeCallback(&state);
                }
            }
            break;
    }

    // Move to the next frame state after processing field.
    ++IEBusRxFrameState;
    if (IEBusRxFrameState >= IEBUS_FRAME_MAX)
    {
        IEBusRxFrameState = IEBUS_FRAME_STATE_IDLE;
    }

    // Reset parity and bit counts.  Do not reset IEBusRxData as it will be used
    // by IEBusTxProcessBit after this function returns.
    IEBusRxParityBit = IEBUS_PARITY_DEFAULT;
    IEBusRxDataBitIndex = 0;
    IEBusRxDataTotalBitCount = IEBusExpectedFieldBitLength[IEBusRxFrameState];

error:

    // Handle and notify error.
    if (IEBUS_ERROR_NONE != error.Error)
    {
        // Notify error.
        if (IEBusOnErrorCallback)
        {
            IEBusOnErrorCallback(&error);
        }

        // Abandon reception.
        IEBusRxFrameState = IEBUS_FRAME_STATE_IDLE;
        if (IEBUS_BUS_STATE_RX == IEBusBusState)
        {
            IEBusBusState = IEBUS_BUS_STATE_IDLE;
        }

        // Report status
        if (IEBusStateChangeCallback)
        {
            IEBusStateChangeType state = { IEBUS_RX_INCOMPLETE, &IEBusRxMessage };
            IEBusStateChangeCallback(&state);
        }
    }
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBUS_DATAIN_INT_VECT

  Description  :  Interupt signal for receive.

  Notes        :  This interrupt toggles at every change.

--------------------------------------------------------------------------------------------------*/
ISR(IEBUS_DATAIN_INT_VECT)
{
    // The pin when to the high level: a new bit is starting.
    if (IEBUS_BUS_IS_HIGH_LEVEL)
    {
        // Reset timer.  We will count the time this spends in the
        // high level to determine what type of bit this is.
        IEBUS_TIMER_RESET;

        // Respond according to state.
        switch (IEBusBusState)
        {
            // This is likely the start of a start bit.  We'll wait
            // until we know it is a start bit before transitioning state.
            case IEBUS_BUS_STATE_IDLE:
                break;

            // We have started transmitting the next bit.
            case IEBUS_BUS_STATE_TX:
                break;

            // We are receiving the next bit.
            case IEBUS_BUS_STATE_RX:

                // Send bata bit as necessary.
                if (IEBusRxShouldSendBit())
                {
                    // "When the master unit reads data from a slave unit, the master unit
                    // outputs a synchronization signal for each bit as it is read.
                    // The slave unit outputs data and a parity bit to the bus according
                    // to the synchronization signal output by the master unit."
                    //
                    // Set bus to high level and set timer to turn it off
                    // to transmit a response bit.  If we intended to transmit
                    // a '1', no need to do anything as the master will output
                    // the '1' and the slave's responsbility is to take control
                    // of the bus to transmit a '0'.
                    if (!IEBusRxGetNextBit())
                    {
                        IEBUS_BUS_SET_HIGH_LEVEL;
                        IEBUS_TIMER_SET_COMPB(IEBusWriteZeroBit);
                    }
                }
                break;
        }
    }

    // The pin when to the low level.
    else
    {
        // Detect start bit.
        if (IEBUS_TCNT > IEBusReadBitIsStart)
        {
            IEBusRxData = IEBUS_DATA_START_BIT;
        }

        // Detect '0'/'1' bit.
        else
        {
            // Insert new bit (0) into existing data and increment bit count.
            IEBusRxData <<= 1;

            // Detect '1' bit.
            if (IEBUS_TCNT < IEBusReadBitIsZero)
            {
                // Set new bit to 1.
                //
                IEBusRxData |= 0x1;
            }
        }

        // Determine state.
        switch (IEBusBusState)
        {
            // We finished receiving a bit.
            case IEBUS_BUS_STATE_IDLE:

                // If this is a start bit, move to receieve state.
                if (IEBUS_DATA_START_BIT == IEBusRxData)
                {
                    IEBusBusState = IEBUS_BUS_STATE_RX;
                    IEBusRxFrameState = IEBUS_FRAME_STATE_START_BIT;
                }
                break;

            // We have finished transmitting the current bit.
            case IEBUS_BUS_STATE_TX:

                // The timer has already been loaded with a Compare A
                // value to create the correct high-level on
                // the bus to ensure the proper bit length.  There
                // is nothing to do here to manage the transmission.
                //

                // Fallthrough: during Tx, we also track the message
                // in the Rx buffer to allow for easy transiton to Rx
                // if we lose in arbitration.

            // We receiving the next bit.
            case IEBUS_BUS_STATE_RX:

                // If in Rx state, set the timer to detect
                if (IEBUS_BUS_STATE_RX == IEBusBusState)
                {
                    IEBUS_TIMER_SET_COMPA(IEBusBusIdleTime);
                }

                // Set data available to process the bit.
                IEBusDataAvailable = TRUE;
                break;
        }
    }
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBUS_TIMER_COMPB_VECT

  Description  :  Timer interrupt for Compare B.

  Notes        :  This interrupt is used for the following conditions:
                    Bus State Idle: Not used
                    Bus State Tx:   Used to transition from high-level to low-level to transmit a bit.
                    Bus State Rx:   Used to transition from high-level to low-level.to transmit the ack.

--------------------------------------------------------------------------------------------------*/
ISR(IEBUS_TIMER_COMPB_VECT)
{
    // Determine state.
    switch (IEBusBusState)
    {
        // no-op.
        case IEBUS_BUS_STATE_IDLE:
            break;

        // If transmitting, send bus to low level to terminate the bit.
        case IEBUS_BUS_STATE_TX:
            IEBUS_BUS_SET_LOW_LEVEL;
            break;

        // if receiving, terminate the data/parity/ack bit when required.
        case IEBUS_BUS_STATE_RX:
            if (IEBusRxShouldSendBit())
            {
                // Set bus to low level to terminate the bit.
                IEBUS_BUS_SET_LOW_LEVEL;
            }
            break;
    }
}

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBUS_TIMER_COMPA_VECT

  Description  :  Timer interrupt for Compare A.

  Notes        :  This interrupt is used for the following conditions:
                    Bus State Idle: Not used
                    Bus State Tx:   Used to begin the transmission of the next bit.
                    Bus State Rx:   Used to detect a bus timeout condition.

--------------------------------------------------------------------------------------------------*/
ISR(IEBUS_TIMER_COMPA_VECT)
{
    // Determine state.
    switch (IEBusBusState)
    {
        // no-op.
        case IEBUS_BUS_STATE_IDLE:
            break;

        // If transmitting, send next bit.
        case IEBUS_BUS_STATE_TX:

            // "The start bit is a signal used to notify the other units of the beginning of data transmission.
            // Before a unit starts data transmission, it outputs a low-level signal (start bit) for a
            // specified duration, then outputs the broadcast bit."
            //
            // "When the unit attempts to output the start bit, another unit may have already output
            // the start bit. In such a case, the unit does not output the start bit, and instead waits
            // for the other unit to stop outputting the start bit. Then, synchronized with the
            // completion of start bit output by the other unit, the unit starts output of the
            // broadcast bit."
            //
            if (IEBUS_FRAME_STATE_START_BIT == IEBusTxFrameState)
            {
                // Take control of the bus if it is currently idle.
                // Otherwise, just frame to existing start bit.
                if (IEBUS_BUS_IS_LOW_LEVEL)
                {
                    IEBUS_BUS_SET_HIGH_LEVEL;
                }

                // Set timers for start bit.
                IEBUS_TIMER_SET_COMPB(IEBusWriteStartBit);
                IEBUS_TIMER_SET_COMPA(IEBusStartBitLength);
            }
            else
            {
                // Start the next bit.
                IEBUS_BUS_SET_HIGH_LEVEL;

                // Determine the next bit and bit lengths.
                IEBUS_TIMER_SET_COMPA(IEBusBitLength);
                if (IEBusTxGetNextBit())
                {
                    IEBUS_TIMER_SET_COMPB(IEBusWriteOneBit);
                }
                else
                {
                    IEBUS_TIMER_SET_COMPB(IEBusWriteZeroBit);
                }
            }
            break;

        // If receiving, a timeout occured.
        // Set data available to process the bit.
        case IEBUS_BUS_STATE_RX:
            IEBusRxData = IEBUS_DATA_TIMEOUT_BIT;
            IEBusDataAvailable = TRUE;
            break;
    }
}

/*--------------------------------------------------------------------------------------------------
                                         End of file.
--------------------------------------------------------------------------------------------------*/
