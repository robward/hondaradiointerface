/*--------------------------------------------------------------------------------------------------

  Name         :  GANetIIDriver.c

  Description  :  GANet-II/IEBus Driver

  Copyright    :  (c) 2012 Crazy Giraffe Software
  
----------------------------------------------------------------------------------------------------*/
#include "GANetIIDriver.h"

// Fwd decl for IEBus callbacks
void GANetHandleStateChange(IEBusStateChangeType* state);
void GANetHandleError(IEBusErrorMessageType* error);
uint8_t GANetWriteCallback(IEBusMessageType* message);
uint8_t GANetReadCallback(IEBusMessageType* message);

// Variables
GANET_DEVICE_TYPE   GANetDeviceType;

// Callbacks function pointers.  Make sure these are initialized to NULL.
GANetStateChange        GANetStateChangeCallback = 0;
GANetOnError            GANetOnErrorCallback = 0;
GANetProcessDataWrite   GANetDataWriteCallback = 0;
GANetProcessDataRead    GANetRxDataCallback = 0;


/*--------------------------------------------------------------------------------------------------

  Name         :  GANetInit

  Description  :  Initialize this library to read/write from the GANet-II Bus.

  Argument(s)  :  deviceType          -> Type of device to emulate
                  stateChangeCallback -> Callback to handle state changes.
                  onErrorCallback     -> Callback to handle error conditions.
                  dataWriteCallback   -> Callback to handle write commnds.
                  dataReadCallback    -> Callback to handle read commands.

  Return value :  TRUE if initialize, FALSE is not.

--------------------------------------------------------------------------------------------------*/
uint8_t GANetInit(
    GANET_DEVICE_TYPE deviceType,
    GANetStateChange stateChangeCallback,
    GANetOnError onErrorCallback,
    GANetProcessDataWrite dataWriteCallback,
    GANetProcessDataRead dataReadCallback
    )    
{
    // Init IEBus
    if (!IEBusInit(GANetHandleStateChange, GANetHandleError, GANetWriteCallback, GANetReadCallback))
    {
        return FALSE;
    }
    
    // Set address
    if (!IEBusSetAddress(deviceType))
    {
        return FALSE;
    }
    GANetDeviceType = deviceType;
    
    // Store callbacks
    GANetStateChangeCallback = stateChangeCallback;
    GANetOnErrorCallback = onErrorCallback;
    GANetDataWriteCallback = dataWriteCallback;
    GANetRxDataCallback = dataReadCallback;    
}

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
void GANetProcess(void)
{
    IEBusProcess(void);
}

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
void GANetHandleStateChange(IEBusStateChangeType* pState)
{
}

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
void GANetHandleError(IEBusErrorMessageType* pError)
{
}

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
uint8_t GANetWriteCallback(IEBusMessageType* pMessage)
{
}

/*--------------------------------------------------------------------------------------------------

  Name         :  GANetProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
uint8_t GANetReadCallback(IEBusMessageType* pMessage)
{
}

