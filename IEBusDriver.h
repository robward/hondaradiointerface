/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusDriver.h

  Description  :  IEBus driver  module.

  Author       :  Rob Ward (info@crazygiraffesoftware.com)

  Copyright    :  (c) 2012 Crazy Giraffe Software

  Credits:     :  Adapated from AvcLanDriver.c
                  by Louis Frigon (info@sigmaobjects.com)
                  (c) 2007 SigmaObjects

--------------------------------------------------------------------------------------------------*/
#include <avr/io.h>

#ifndef _IEBUS_DRIVER_H_
#define _IEBUS_DRIVER_H_

#define IEBUS_VERSION "0.2"

// IO Definitions.
#define IEBUS_RX_PIN            PIND
#define IEBUS_RX_IO             PIND2
#define IEBUS_RX_DDR            DDRD
#define IEBUS_DATAIN_INT_VECT   INT0_vect

#define IEBUS_TX_PORT           PORTD
#define IEBUS_TX_IO             PORTD3
#define IEBUS_TX_DDR            DDRD

// Timer definitions. Note that the timing constants below have to be
// within range of the max timer count.  An 8 bit timer works fine for
// IEBus mode 1 (6 or 6.29 Mhz) with a MCU speed of 8 Mhz - 20 Mhz.
// For IEBus mode 0 (6 or 6.29 Mhz), a 16-bit timer is needed.
#define IEBUS_TCNT              TCNT1
#define IEBUS_OCRAH             OCR1AH
#define IEBUS_OCRAL             OCR1AL
#define IEBUS_OCRBH             OCR1BH
#define IEBUS_OCRBL             OCR1BL
#define IEBUS_TCCRA             TCCR1A
#define IEBUS_TCCRB             TCCR1B
#define IEBUS_TCCRB_CLK         _BV(CS10);
#define IEBUS_TCCRB_CLK8        _BV(CS11);
#define IEBUS_TIMER_MAX_VALUE   0xffff

#define IEBUS_TIMER_COMPA_VECT  TIMER1_COMPA_vect
#define IEBUS_TIMER_COMPB_VECT  TIMER1_COMPB_vect

// Timing: these are for Mode 1, 6 .29Mhz
// these are known to work with both Honda (GANET II)
// and Toyota (AVC-Lan).  It should also work with
// Pioneer IP-BUS.
#define IEBUS_START_BIT_LENGTH_MS       186
#define IEBUS_START_BIT_HIGH_LENGTH_MS  168
#define IEBUS_BIT_LENGTH_MS              39
#define IEBUS_BIT_1_HIGH_LENGTH_MS       20
#define IEBUS_BIT_0_HIGH_LENGTH_MS       32
#define IEBUS_MAX_DATA_LENGTH_BYTES      32

// Broadcast options.
typedef enum
{
    IEBUS_BROADCAST_NONE = 0x0,
    IEBUS_BROADCAST_ALL = 0x1,
    IEBUS_BROADCAST_GROUP = 0x81,
}
IEBUS_BROADCAST_TYPE;

// Special addresses.  Broadcast address is used for
// global broadcast to all devices.  Invalid address is
// a sentinal value for an invalid address.  This is most
// commonly used as the recieve address when operating in
// promiscuous mode.
// My address is a sentinal value for my address.  This is most
// commonly used as the master address when sending a message.
#define IEBUS_BROADCAST_ADDR            0xFFF
#define IEBUS_INVALID_ADDR              0xF000
#define IEBUS_MY_ADDR                   0xE000

// Control field values contained in IEBusMessageType.
typedef enum
{
    // The defined control values.
    IEBUS_CONTROL_READ_STATUS = 0x0,
    IEBUS_CONTROL_READ_DATA_LOCK = 0x3,
    IEBUS_CONTROL_READ_LOCK_ADDR_LO = 0x4,
    IEBUS_CONTROL_READ_LOCK_ADDR_HI = 0x5,
    IEBUS_CONTROL_READ_STATUS_UNLOCK = 0x6,
    IEBUS_CONTROL_READ_DATA_NO_LOCK = 0x7,
    IEBUS_CONTROL_WRITE_CMD_LOCK = 0xa,
    IEBUS_CONTROL_WRITE_DATA_LOCK = 0xb,
    IEBUS_CONTROL_WRITE_CMD_NO_LOCK = 0xe,
    IEBUS_CONTROL_WRITE_DATA_NO_LOCK = 0xf,

    // These are the most commonly used control values.
    IEBUS_CONTROL_GENERIC_WRITE = 0x1f,
}
IEBUS_CONTROL_TYPE;

// Frame fields contained in IEBusErrorMessageType for IEBusOnError callback.
typedef enum
{
    IEBUS_START_BIT_FIELD = 0,
    IEBUS_BROADCAST_FIELD,
    IEBUS_MASTER_ADDR_FIELD,
    IEBUS_SLAVE_ADDR_FIELD,
    IEBUS_CONTROL_FIELD,
    IEBUS_DATA_SIZE_FIELD,
    IEBUS_DATA_FIELD,
}
IEBUS_FIELD_TYPE;

// Error types contained in IEBusErrorMessageType for IEBusOnError callback.
typedef enum
{
    // No error
    IEBUS_ERROR_NONE = 0,

    // The bus timed-out, most likely an abandoned transmission.  Field indicates the failed field.
    IEBUS_ERROR_TIMEOUT,

    // Receive parity error, Field indicates the failed field.
    //  If field is IEBUS_DATA_FIELD, Data[0] contains the data byte index that failed.
    IEBUS_ERROR_PARITY,

    // Receive invalid control, Field is:
    //  IEBUS_BROADCAST_FIELD indicates receiving a read control for a broadcast address.
    //  IEBUS_CONTROL_ indicates invalid control data, Data[0] contains received control.
    IEBUS_ERROR_INVALID_CONTROL,

    // Ignoring control due to locking.
    //  Field is IEBUS_CONTROL_, Data[0] contains received control.
    IEBUS_ERROR_LOCKED,

    // Transmit collision error, Field indicates the failed field.
    // The master has lost the bus in arbitration and will resend the message
    // once the bus is available.  No
    IEBUS_ERROR_COLLISION,

    // Transmit error, Field indicates the unsupported field:
    //  IEBUS_START_BIT_FIELD indicates a NULL message.
    //  IEBUS_MASTER_ADDR_FIELD indicates an invalid master address and a missing receiver address.
    //  IEBUS_SLAVE_ADDR_FIELD indicates an invalid slave address.
    //  IEBUS_DATA_SIZE_FIELD indicates sending less than 0 bytes, more than 256 bytes or reading
    //      a message with more than IEBUS_MAX_DATA_LENGTH_BYTES bytes.
    IEBUS_ERROR_UNSUPPORTED_MSG,

    // Transmit was unacklowedged, Field indicates the unacknowledged field.
    // The current transmission will abandoned.
    IEBUS_ERROR_UNACKNOWLEDGED_MSG,

    // Transmit of a data byte was unacklowedged, the master will resend the data byte.
    //  If Field is IEBUS_DATA_FIELD, Data[0] contains the data byte index that failed.
    IEBUS_ERROR_RESENDING,

    // There is a transmission in progress.  Field is IEBUS_START_BIT_FIELD.
    IEBUS_TX_IN_PROGRESS,

    // The timing constants are out of range; this bitrate is unsupported.
    // Field is
    IEBUS_ERROR_OUT_OF_RANGE,

    // The read callback failed, reception abandoned.
    // Field is
    IEBUS_ERROR_READ_FAILED,
}
IEBUS_ERROR_TYPE;

// Error types contained in IEBusStateChangeType for IEBusStateChange callback.
typedef enum
{
    // Bus is idle
    IEBUS_IDLE = 0,

    // Data receive has started.
    IEBUS_RX_BEGIN,

    // Data receive is complete.
    IEBUS_RX_COMPLETE,

    // Data receive is partially complete, more frame are required.
    IEBUS_RX_PARTIAL,

    // Data receive has stopped; an error occured.
    IEBUS_RX_INCOMPLETE,

    // Data transmit has started.
    IEBUS_TX_BEGIN,

    // Data transmit is partially complete, more frame are required.
    IEBUS_TX_PARTIAL,

    // Data transmit receive is complete.
    IEBUS_TX_COMPLETE,

    // Data receive has stopped; an error occured.
    IEBUS_TX_INCOMPLETE,
}
IEBUS_STATE_TYPE;

// Message
typedef struct
{
    IEBUS_BROADCAST_TYPE    Broadcast;
    uint16_t                MasterAddress;
    uint16_t                SlaveAddress;
    IEBUS_CONTROL_TYPE      Control;
    uint16_t                DataSize;
    uint8_t                 Data[256];
} IEBusMessageType;

typedef struct
{
    IEBUS_ERROR_TYPE    Error;
    IEBUS_FIELD_TYPE    Field;
    uint8_t             Data[1];
} IEBusErrorMessageType;

typedef struct
{
    IEBUS_STATE_TYPE    State;
    IEBusMessageType*   pMessage;
} IEBusStateChangeType;


// Callback prototypes
typedef void (*IEBusStateChange) (IEBusStateChangeType* pState);
typedef void (*IEBusOnError) (IEBusErrorMessageType* pError);
typedef uint8_t (*IEBusProcessDataWrite) (IEBusMessageType* pMessage);
typedef uint8_t (*IEBusProcessDataRead) (IEBusMessageType* pMessage);

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusInit

  Description  :  Initialize this library to read/write from the IEBbus.

  Argument(s)  :  stateChangeCallback -> Callback to handle state changes.
                  onErrorCallback     -> Callback to handle error conditions.
                  dataWriteCallback   -> Callback to handle write commnds.
                  dataReadCallback    -> Callback to handle read commands.

                     IEBusRxInProgress is called with inProgress == FALSE and no message

                  TRUE  -> Message was read:
                     IEBusProcessDataWrite is called when addressed to us and control indicates read.
                     IEBusProcessDataRead is called when addressed to us and control indicates read.
                     IEBusRxInProgress is called when inProgress == FALSE regardless of slave address.

  Notes        : This function may call IEBusOnError to indicate a communication error.

  Return value :  TRUE if initialize, FALSE is not.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusInit(
    IEBusStateChange stateChangeCallback,
    IEBusOnError onIdleError,
    IEBusProcessDataWrite dataWriteCallback,
    IEBusProcessDataRead dataReadCallback
    );

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusSetAddress

  Description  :  Set the address for which to send and receive messages.

  Argument(s)  :  address -> address for which to receive messages.

  Return value :  TRUE if address set, FALSE if not.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusSetAddress(uint16_t address);

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusSendMessage

  Description  :  Sends the message to the IEBus.

  Argument(s)  :  pTxMessage -> message to send.

  Return value :  (bool) -> TRUE if successful queued for sending, else FALSE.

--------------------------------------------------------------------------------------------------*/
uint8_t IEBusSendMessage (IEBusMessageType* pTxMessage);

/*--------------------------------------------------------------------------------------------------

  Name         :  IEBusProcess

  Description  :  Process the transmit and receive state machines.

  Argument(s)  :  None

  Return value :  None

--------------------------------------------------------------------------------------------------*/
void IEBusProcess(void);

#endif // _IEBUS_DRIVER_H_

/*--------------------------------------------------------------------------------------------------
                                         End of file.
--------------------------------------------------------------------------------------------------*/
