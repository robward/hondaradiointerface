/*--------------------------------------------------------------------------------------------------

  Name         :  GANetIIMain.c

  Description  :  This is the GANet-II side of the honda radio interface

  MCU          :  ATmega328 @ 20 MHz

  Author       :  Rob Ward (info@crazygiraffesoftware.com)

  Copyright    :  (c) 2012 Crazy Giraffe Software

--------------------------------------------------------------------------------------------------*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h> 
//#include <stdio.h>
#include "IEBusDriver.h"
#include "HardSerial.h"

#define FIRMWARE_VERSION    "v0.5"
#define FIRMWARE_DATE       __DATE__

// Chip:
// ATMega328
//
// PB0:        NC
// PB1:        System Acc from GANet-II
// SS/PB2:     iPod Select (high)
// MOSI/PB3:   MOSI for ICSP
// MISO/PB4:   MISO for ICSP
// SCLK/PB5:   SCLCK for ICSP
// XTAL1/PB6:  Crystal
// XTAL2/PB7:  Crystal
// ...
// PC0:        iPod Present (3.3v)
// PC1:        iPod RX LED (low)
// PC2:        iPod TX LED (low)
// PC3:        NC
// SDA/PC4:    NC
// SCL/PC5:    NC
// RST/PC6:    Reset
// ...
// RXD/PD0:    iPod RX
// TXD/PD1:    iPod TX
// PD2/INT0:   IEBus RX
// PD3/INT1:   IEBus TX
// PD4:        NC
// PD5:        IEBus Error LED (low)
// PD6:        IEBus RX LED (low)
// PD7:        IEBus TX LED (low)

// LED
#define LED_IPOD_TX     PORTC1
#define LED_IPOD_RX     PORTC2
#define LED_IEBUS_ERR   PORTD5
#define LED_IEBUS_RX    PORTD6
#define LED_IEBUS_TX    PORTD7

// True/false
#ifndef FALSE
#define FALSE                   0
#endif
#ifndef TRUE
#define TRUE                    (!FALSE)
#endif

// Fwd Decl
void InitPorts(void);
void SetLed(uint8_t led, uint8_t state);

//
// Main
//
int main(void)
{
    // Init ports
    InitPorts();
    
    // Init GANet-II
    GANetInit(GANET_DEVICE_CD);
    
    // Init iPod port
    (void)SerialInit(IPOD_BAUD_RATE);

    // Init iPod
    iPodInit(IPOD_MODE_SIMPLE, 0);

    // enable interrupts
    // used by serial + sample & report timers
    sei();

    // Loop forever
    for (;;)
    {
    }

    return 0;
}

//
// Initialize ports
//
void InitPorts(void)
{
    // Disable pullups
    MCUCR |= _BV(PUD);
    
    // Port B
    // PB0:        NC
    // PB1:        System Acc from GANet-II
    // SS/PB2:     iPod Select (high)
    DDRB &= ~_BV(PORTB1);
    DDRB |= _BV(PORTB2);
    PORTB &= ~_BV(PORTB2);
    
    // port C
    // PC0:        iPod Present (3.3v)
    // PC1:        iPod RX LED (low)
    // PC2:        iPod TX LED (low)
    DDRC &= ~_BV(PORTC0);
    DDRC |= _BV(PORTC1) | _BV(PORTC2);
    PORTC |= _BV(PORTC1) | _BV(PORTC2);
    
    // Port D
    // PD5:        IEBus Error LED (low)
    // PD6:        IEBus RX LED (low)
    // PD7:        IEBus TX LED (low)
    DDRD |= _BV(PORTD5) | _BV(PORTD6) | _BV(PORTD7);
    PORTD |= _BV(PORTD5) | _BV(PORTD6) | _BV(PORTD7);
    
    // Re-enable pullups
    MCUCR &= ~_BV(PUD);
}

//
// Leds
//
void SetLed(uint8_t led, uint8_t state)
{
    if (led == PORTC1 || led == LED_IPOD_RX)
    {
        if (state)
        {
            PORTC &= ~_BV(led);
        }
        else
        {
            PORTC |= _BV(led);
        }
    }
    else
    {
        if (state)
        {
            PORTD &= ~_BV(led);
        }
        else
        {
            PORTD |= _BV(led);
        }
    }
}
